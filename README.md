# Play SQL Dialects #

This library contains:

* The **playsql-dialects-plugin**, which contains examples of dialects for Play SQL Spreadsheets (namely the full PostgreSQL and HSQL dialects, and the partial MySQL and Oracle dialects).
* And more importantly, a library to manage schemas, tables, columns and data in a database.

Both are used by Play SQL Spreadsheets.

## Example of the DDL API usage in Java ##

If you develop your own project based on this API, your code will look like the [DialectTest.java](https://bitbucket.org/aragot/play-sql-dialects/src/master/playsql-dialects-plugin/src/test/java/DialectTest.java) file:

```
#!java
        if (rDialect.hasSchema("space_test")) {
            rDialect.dropSchema("space_test");
        }

        rDialect.createSchema("space_test");
        rDialect.setDefaultSchema("space_test");
        DDLColumn colID = new DDLColumn("ID", "#", null, null, null, DDLColumn.DataType.INTEGER, null, null, null);
        DDLColumn colPOSITION = new DDLColumn("POSITION", "POSITION", null, null, null, DDLColumn.DataType.TEXT, null, null, null);
        DDLColumn column1 = new DDLColumn("COL1", "Column 1", null, "integer-renderer", null, DDLColumn.DataType.INTEGER, null, null, null);
        DDLColumn column2 = new DDLColumn("COL2", "Column 2", null, "as-string", null, DDLColumn.DataType.TEXT, null, null, null);
        DDLTable table = new DDLTable(true, "test1", "Test 1", colID, colPOSITION, column1, column2);
        rDialect.createTable(table);
```


### How do I get set up? ###

Use ```mvn clean install``` at the root.

Use the JUnit tests (DialectTest.java) to test your developments. Create your own dialect and submit your contribution so we publish it.

### Contribution guidelines ###

* If you create new dialects / improve existing ones and we're satisfied with the improvements, we'll publish it in our original plugin,
* If you want your dialect to be "verified", please write tests,
* DBMSs have fundamental differences. We make no promises you will be able to write a dialect for your own database.

However:
* Please be aware that the dialect API changes at almost each version. If you define your own dialect for personal use, you may need to update it often. If you submit it to us and it's easy enough to maintain, we'll help you.

### Who do I talk to? ###

* For code, just submitting a Pull Request is enough,
* Otherwise Adrien Ragot (see Play SQL website)