package com.playsql.dialect.model.monitoring;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

/**
 * Represents the details of an SQL cursor, as to be displayed in the front-end.
 */
public class Cursor {
    private String name;
    private String statement;
    private String is_holdable;
    private String is_binary;
    private String is_scrollable;
    private String creation_time;

    //<editor-fold desc="Wires">
    public Cursor() {
    }

    public Cursor(String name, String statement, String is_holdable, String is_binary, String is_scrollable, String creation_time) {
        this.name = name;
        this.statement = statement;
        this.is_holdable = is_holdable;
        this.is_binary = is_binary;
        this.is_scrollable = is_scrollable;
        this.creation_time = creation_time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public String getIs_holdable() {
        return is_holdable;
    }

    public void setIs_holdable(String is_holdable) {
        this.is_holdable = is_holdable;
    }

    public String getIs_binary() {
        return is_binary;
    }

    public void setIs_binary(String is_binary) {
        this.is_binary = is_binary;
    }

    public String getIs_scrollable() {
        return is_scrollable;
    }

    public void setIs_scrollable(String is_scrollable) {
        this.is_scrollable = is_scrollable;
    }

    public String getCreation_time() {
        return creation_time;
    }

    public void setCreation_time(String creation_time) {
        this.creation_time = creation_time;
    }
    //</editor-fold>
}
