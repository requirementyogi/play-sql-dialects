package com.playsql.dialect.model.audittrail;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

/** Metadata about a column, saved by the audit trail */
public class ColumnMetadata {
    public String label;
    public String formula;
    public String renderer;
    public String viewRenderer;
    public String fkDisplayField;
    public String fkSearchField;

    public ColumnMetadata(){}

    public ColumnMetadata(String label, String formula, String renderer, String viewRenderer, String fkDisplayField, String fkSearchField) {
        this.label = label;
        this.formula = formula;
        this.renderer = renderer;
        this.viewRenderer = viewRenderer;
        this.fkDisplayField = fkDisplayField;
        this.fkSearchField = fkSearchField;
    }
}
