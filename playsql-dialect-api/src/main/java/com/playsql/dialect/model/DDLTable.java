package com.playsql.dialect.model;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

/**
 * Details of a table in the database. It's one of the main beans used by a dialect.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DDLTable implements Serializable
{
    /** Whether the table is a PlaySQL table */
    private boolean readwrite;
    private String name;
    private String label;
    private boolean child = false;
    private List<DDLColumn> columns = Lists.newArrayList();
    private List<TablePerm> perms = Lists.newArrayList();

    /** To override the default "No data" message */
    private String customEmptyMessage;

    @XmlElement(name = "totals")
    private List<TotalColumn> totals;

    @XmlElement(name = "columnOrder")
    private List<String> columnOrder;

    //<editor-fold desc="Constructors">
    public DDLTable(boolean readwrite, String name, String label, DDLColumn... columns)
    {
        this.readwrite = readwrite;
        this.name = name;
        this.label = label;
        if (columns != null)
            this.columns.addAll(Lists.newArrayList(columns));
    }
    public DDLTable(boolean readwrite, String name, String label, List<DDLColumn> columns)
    {
        this.name = name;
        this.label = label;
        this.readwrite = readwrite;
        if (columns != null)
            this.columns.addAll(columns);
    }

    public DDLTable(boolean readwrite, String name, DDLColumn... columns)
    {
        this.readwrite = readwrite;
        this.name = name;
        if (columns != null)
            this.columns.addAll(Lists.newArrayList(columns));
    }

    public DDLTable(String name) {
        this.readwrite = true;
        this.name = name;
    }

    public DDLTable() {
        // JAXB no-arg constructor
    }

    /** Cloning construction
     * Example: table.copy(null)
     * */
    public DDLTable copy(DDLTable result) {
        if (result == null) result = new DDLTable();
        result.readwrite = this.readwrite;
        result.name = this.name;
        result.label = this.label;
        result.customEmptyMessage = this.customEmptyMessage;

        if (this.perms == null) {
            result.perms = null;
        } else {
            // TablePerm is an enum, no cloning needed
            result.perms = Lists.newArrayList(this.perms);
        }

        if (this.totals == null) {
            result.totals = null;
        } else {
            result.totals = Lists.newArrayList();
            for (TotalColumn totalColumn : this.totals) {
                result.totals.add(totalColumn.copy(null));
            }
        }

        if (this.columns == null) {
            result.columns = null;
        } else {
            result.columns = Lists.newArrayList();
            for (DDLColumn column : this.columns)
                result.columns.add(column.copy(null));
        }

        if (columnOrder == null) {
            result.columnOrder = null;
        } else {
            result.columnOrder = Lists.newArrayList(columnOrder);
        }
        return result;
    }

    //</editor-fold>

    /**
     * Search for a column. Unspecified if two columns have the same name.
     * Does not return synthetic columns.
     *
     * @return the column, or null if not found
     * */
    public DDLColumn getColumnByName(String columnName) {
        return Iterables.find(columns, Predicates.and(DDLColumn.withName(columnName), DDLColumn.andNonSynthetic()), null);
    }

    public DDLColumn getPkColumn() {
        for (DDLColumn column : columns)
            if (column.isStatusPK())
                return column;
        return null;
    }

    //<editor-fold desc="Wires">
    public String getName()
    {
            return name;
    }

    /** The columns. Passed by reference, the list can be modified. */
    public List<DDLColumn> getColumns()
    {
        return columns;
    }

    public boolean addColumn(DDLColumn e)
    {
        return columns.add(e);
    }

    public void setName(String name) {
        this.name = name;
    }

    public DDLTable setColumns(List<DDLColumn> columns) {
        this.columns = columns;
        return this;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<TotalColumn> getTotals() {
        return totals;
    }

    public DDLTable setTotals(List<TotalColumn> totals) {
        this.totals = totals;
        return this;
    }

    public String getCustomEmptyMessage() {
        return customEmptyMessage;
    }

    public DDLTable withCustomEmptyMessage(String customEmptyMessage) {
        this.customEmptyMessage = customEmptyMessage;
        return this;
    }

    public List<TablePerm> getPerms() {
        return perms;
    }

    public boolean isReadwrite() {
        return readwrite;
    }

    public void setReadwrite(boolean readwrite) {
        this.readwrite = readwrite;
    }

    public DDLTable withPerms(List<TablePerm> perms) {
        this.perms = perms;
        return this;
    }

    public DDLTable withPerms(TablePerm... perms) {
        Collections.addAll(this.perms, perms);
        return this;
    }

    public void setPerms(List<TablePerm> perms) {
        this.perms = perms;
    }

    //</editor-fold>


    public List<String> getColumnOrder() {
        return columnOrder;
    }

    /**
     * Sets an order for the columns.
     * @param columnOrder the order, by column name
     * @param synchronize whether the columns should be reordered in accordance with this property
     * @return this
     */
    public DDLTable withColumnOrder(List<String> columnOrder, boolean synchronize) {
        this.columnOrder = columnOrder;

        if (synchronize) {
            if (this.columnOrder == null || columnOrder.isEmpty()) {
                this.columnOrder = null;
            } else {
                // The algo puts each column at the beginning, starting from the last, so they're in order
                ListIterator<String> iterator = columnOrder.listIterator(columnOrder.size());
                while (iterator.hasPrevious()) {
                    String name = iterator.previous();
                    DDLColumn column = getColumnByName(name);
                    if (column == null) {
                        iterator.remove();
                    } else {
                        // Move to first
                        columns.remove(column);
                        columns.add(0, column);
                    }
                }
            }
        }
        return this;
    }

    @Override
    public String toString() {
        return String.format("DDLTable{%s/%s, %d columns}", label, name, columns == null ? -1 : columns.size());
    }
}
