package com.playsql.dialect.model;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

/**
 * Details of a column in the database. It's one of the main beans used by a dialect.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DDLColumn implements Serializable
{
    public static final String COL_NAME_PLAYSQL_ID = "PLAYSQL_ID";
    public static final String COL_NAME_PLAYSQL_POSITION = "PLAYSQL_POSITION";

    /** Contains the unique ID of a record.
     * For a Play SQL table, must exist as the first column with uppercase
     * name. Must be integer, autoincremented.
     */
    public static final String COL_NAME_ID = "ID";

    /**
     * Contains some metadata information about the row, in JSON format.
     * For a Play SQL table, must exist as the second column with uppercase
     * name. Must be unlimited text.
     *
     * The non-intuitive name comes from legacy: It used to only contain the
     * position of the row, but we've extended it to all metadata.
     */
    public static final String COL_NAME_POSITION = "POSITION";

    public enum DataType
    {
        VARCHAR,

        /** Varchar with infinite length. POSTGRES only */
        TEXT,

        /** Signed, 4 bytes */
        INTEGER,

        BIG_INTEGER,

        BOOLEAN,
        
        DATE,
        
        TIME,
        
        TIMESTAMP,
        
        INTERVAL,
        
        /** 2 digits after comma */
        NUMERIC,
        
        DOUBLE,

        BYTEA,

        // If there's no known type for that (or not implemented yet)
        UNKNOWN;
    }
    public enum Option {
        /** Denotes the mandatory Identifier column,
         * which must be named ID and be INTEGER. For now. */
        IDENTITY,
        NOT_NULL
    }

    private DataType type;
    private Integer length;
    private String name;
    /** Convenience name which isn't used for DDL */
    private String label;

    /** How to calculate the value we display. By default, we obviously use the field's name, but
     * that's an opportunity to let the user use formulas. The formula is always expressed using Formulas.g4 language.
     *
     * If the status is "synthetic" or "not materialized", then the formula is directly expressed in SQL (this is the case iff
     * this DDLColumn represents the target of an FK's display fields).
     */
    @Nullable
    private String formula;

    /** Aka 'the edit renderer', used for the back-end and for edition. May be used for viewing, unless a viewRenderer is
     * defined */
    private String renderer;

    /** Optional: The renderer for viewing, when there's a formula returning a different type than the
     * edit renderer.
     */
    @Nullable
    private String viewRenderer;
    private Option[] options;
    /** The CSS width for presentation */
    private Integer width;
    /** Properties to define the format of the column */
    private Map<String, String> formats = Maps.newHashMap();

    /** True if it's a virtual column. Default is false. Virtual columns aren't present in the SQL query but are still
     * emulated when returned to the client. This is designed for PLAYSQL_ID and PLAYSQL_POSITION. The name could
     * be duplicate with other names in the query, if the user particularly enjoys having a duplicate column name;
     */
    private boolean statusSynthetic;

    /** Default is true for any column of the table. False if it doesn't represent a database column. If false,
     * it's expected that there's a formula to determine the front-end value. The back-end value will be null,
     * since there is no underlying column. The formula will be in SQL.
      */
    private boolean statusMaterialized = true;
    /** True if the column is part of the key of the spreadsheet. For now we only expect 1 key. */
    private boolean statusPK;
    /** True if the column contains the position information for the row */
    private boolean statusPosition;

    /**
     * The details of the INTERVAL type:
     * <interval type> ::= INTERVAL <interval qualifier>
     * 
     * <interval qualifier> ::= <start field> TO <end field> | <single datetime field>
     * 
     * <start field> ::= <non-second primary datetime field> [ <left paren> <interval leading field precision> <right paren> ]
     * 
     * <end field> ::= <non-second primary datetime field> | SECOND [ <left paren> <interval fractional seconds precision> <right paren> ]
     * 
     * <single datetime field> ::= <non-second primary datetime field> [ <left paren> <interval leading field precision> <right paren> ] | SECOND [ <left paren>
     * <interval leading field precision> [ <comma> <interval fractional seconds precision> ] <right paren> ]
     * 
     * <primary datetime field> ::= <non-second primary datetime field> | SECOND
     * 
     * <non-second primary datetime field> ::= YEAR | MONTH | DAY | HOUR | MINUTE
     * 
     * <interval fractional seconds precision> ::= <unsigned integer>
     * 
     * <interval leading field precision> ::= <unsigned integer>
     * 
     * Examples of INTERVAL type definition:
     * 
     * INTERVAL YEAR TO MONTH
     * INTERVAL YEAR(3)
     * INTERVAL DAY(4) TO HOUR
     * INTERVAL MINUTE(4) TO SECOND(6)
     * INTERVAL SECOND(4,6)
     */
    private String intervalDetail = null;

    private ForeignKey fk = null;

    public DDLColumn(String name, String label, @Nullable String formula, String renderer, String viewRenderer, DataType type, Integer length, Integer width, Map<String, String> formats)
    {
        this(name, label, type);
        this.formula = formula;
        this.renderer = renderer;
        this.viewRenderer = viewRenderer;
        this.length = length;
        this.width = width;
        this.formats = formats;
    }

    public DDLColumn(String name, String label, DataType type, Integer length)
    {
        this(name, label, type);
        this.length = length;
    }

    public static DDLColumn interval(String name, String label, String intervalDetail)
    {
        DDLColumn column = new DDLColumn(name, label, DataType.INTERVAL);
        column.intervalDetail = intervalDetail;
        return column;
    }

    public DDLColumn(String name, String label, DataType type, Option... options)
    {
        super();
        this.name = name;
        this.label = label;
        this.type = type;
        this.options = options;
    }

    public DDLColumn() {
        // JAXB default constructor
    }

    // Deep-cloning constructor
    public <T extends DDLColumn> T copy(T result) {
        if (result == null) result = (T) new DDLColumn();
        result.setName(this.name);
        result.setLabel(this.label);
        result.setType(this.type);
        result.setLength(this.length);
        if (this.options != null) {
            result.setOptions(Arrays.copyOf(this.options, this.options.length));
        } else {
            result.setOptions(null);
        }
        result.setFormula(this.formula);
        result.setRenderer(this.renderer);
        result.setViewRenderer(this.viewRenderer);
        result.setIntervalDetail(this.intervalDetail);
        result.setWidth(this.width);
        result.setFormats(this.formats != null ? Maps.newHashMap(this.formats) : Maps.<String, String>newHashMap());
        result.setStatusPK(this.statusPK);
        result.setStatusPosition(this.statusPosition);
        result.setStatusSynthetic(this.statusSynthetic);
        result.setStatusMaterialized(this.statusMaterialized);
        result.setFk(ForeignKey.copy(this.fk));
        return result;
    }

    //<editor-fold desc="Bean wires">
    public String getName()
    {
        return name;
    }

    public DDLColumn.DataType getType()
    {
        return type;
    }

    public String getIntervalDetail()
    {
        return intervalDetail;
    }

    public void setIntervalDetail(String intervalDetail) {
        this.intervalDetail = intervalDetail;
    }

    public void setType(DDLColumn.DataType type)
    {
        this.type = type;
    }

    public void setLength(Integer length)
    {
        this.length = length;
    }

    public Integer getLength()
    {
        return length;
    }

    public DDLColumn setName(String name) {
        this.name = name;
        return this;
    }

    public Option[] getOptions() {
        return options;
    }

    public DDLColumn setOptions(Option... options) {
        this.options = options;
        return this;
    }

    public boolean has(Option test) {
        if (options != null)
            for (Option option : options)
                if (option == test) return true;
        return false;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getRenderer() {
        return renderer;
    }

    public void setRenderer(String renderer) {
        this.renderer = renderer;
    }

    @Nullable
    public String getViewRenderer() {
        return viewRenderer;
    }

    public DDLColumn setViewRenderer(@Nullable String viewRenderer) {
        this.viewRenderer = viewRenderer;
        return this;
    }

    public Map<String, String> getFormats() {
        return formats;
    }

    public void setFormats(Map<String, String> formats) {
        this.formats = formats;
    }

    public String getFormula() {
        return formula;
    }

    public DDLColumn setFormula(String formula) {
        this.formula = formula;
        return this;
    }

    //</editor-fold>

    public static Predicate<DDLColumn> withName(final String name) {
        return new Predicate<DDLColumn>(){
            @Override
            public boolean apply(@Nullable DDLColumn input) {
                return input != null && input.getName().equals(name);
            }
        };
    }
    public static Predicate<DDLColumn> withNameInsensitive(final String name) {
        return new Predicate<DDLColumn>(){
            @Override
            public boolean apply(@Nullable DDLColumn input) {
                return input != null && input.getName().equalsIgnoreCase(name);
            }
        };
    }

    public static Predicate<DDLColumn> andNonSynthetic() {
        return new Predicate<DDLColumn>(){
            @Override
            public boolean apply(@Nullable DDLColumn input) {
                return input == null || !input.isStatusSynthetic();
            }
        };
    }

    public static Function<DDLColumn, String> getColumnNames() {
        return new Function<DDLColumn, String>() {
            @Override
            public String apply(@Nullable DDLColumn input) {
                return input.getName();
            }
        };
    }

    public boolean isStatusSynthetic() {
        return statusSynthetic;
    }

    public DDLColumn setStatusSynthetic(boolean statusSynthetic) {
        this.statusSynthetic = statusSynthetic;
        return this;
    }

    /**
     * All columns are materialized. Only columns used for SQL calculations are virtual with no underlying column.
     * @return true in general, false when there's no column with name 'name'.
     */
    public boolean isStatusMaterialized() {
        return statusMaterialized;
    }

    public DDLColumn setStatusMaterialized(boolean statusMaterialized) {
        this.statusMaterialized = statusMaterialized;
        return this;
    }

    public boolean isStatusPK() {
        return statusPK;
    }

    public DDLColumn setStatusPK(boolean statusPK) {
        this.statusPK = statusPK;
        return this;
    }

    public boolean isStatusPosition() {
        return statusPosition;
    }

    public DDLColumn setStatusPosition(boolean statusPosition) {
        this.statusPosition = statusPosition;
        return this;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public ForeignKey getFk() {
        return fk;
    }

    public DDLColumn setFk(ForeignKey fk) {
        this.fk = fk;
        return this;
    }

    public boolean hasFk() {
        return fk != null;
    }

    public boolean hasFkWithoutUI() {
        return fk != null && !fk.isWithUI();
    }

    public static DDLColumn getPositionColumnDDL() {
        DDLColumn position = new DDLColumn(COL_NAME_POSITION, "Position", DataType.TEXT);
        position.setStatusPosition(true);
        return position;
    }

    public static DDLColumn getIdColumnDDL() {
        return new DDLColumn(
            DDLColumn.COL_NAME_ID,
            "#", DataType.INTEGER,
            Option.IDENTITY)
            .setStatusPK(true);
    }

    public static DDLColumn getPK(List<DDLColumn> columns) {
        if (columns != null)
            for (DDLColumn column : columns)
                if (column.isStatusPK())
                    return column;
        return null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("DDLColumn");
        if (statusSynthetic) sb.append(" synthetic");
        sb.append("{").append(name).append(" ");
        if (statusPK) sb.append(" #statusPK ");
        if (statusPosition) sb.append(" #statusPosition ");

        if (label != null) sb.append('"').append(label).append("\" ");
        sb.append(type);
        if (length != null) sb.append(' ').append(length);
        if (options != null) sb.append(' ').append(StringUtils.join(options, " "));
        if (formula != null) sb.append(" =").append(formula);
        if (fk != null) sb.append(" fk->").append(fk.getTargetTable());
        sb.append("}");
        return format(sb.toString());
    }
}