package com.playsql.dialect.model.monitoring;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang.StringUtils;

import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

/**
 * Represents the details of running query at the SQL level, as to be displayed in the front-end.
 */
public class RunningQuery {
    private String pid;
    private String state;
    private String query;
    private Timestamp query_start;
    private String datname;
    private String usesysid;
    private String usename;
    private String application_name;
    private String client_addr;
    private String client_hostname;
    private String client_port;
    private Timestamp backend_start;
    private Timestamp xact_start;
    private Timestamp stateChange;
    private Timestamp sysdate;

    //<editor-fold desc="Description">
    public RunningQuery() {
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Timestamp getQuery_start() {
        return query_start;
    }

    public RunningQuery setQuery_start(Timestamp query_start) {
        this.query_start = query_start;
        return this;
    }

    private static final long NINE_SECONDS = 9000L;
    private static final long THREE_MINUTES = 3L * 60L * 1000L;
    private static final long THREE_HOURS = 3L * 3600L * 1000L;
    private static final long THIRTY_HOURS = 30L * 3600L * 1000L;

    public String getDuration(Timestamp stmp) {
        if (stmp == null) return "";
        long now = sysdate.getTime();
        long queryStart = stmp.getTime();
        long queryDuration = now - queryStart;
        if (queryDuration < 0)
            return "Started " + stmp.toString();
        if (queryDuration < NINE_SECONDS)
            return TimeUnit.MILLISECONDS.toMillis(queryDuration) + "ms ago";
        if (queryDuration < THREE_MINUTES)
            return TimeUnit.MILLISECONDS.toSeconds(queryDuration) + "s ago";
        if (queryDuration < THREE_HOURS)
            return TimeUnit.MILLISECONDS.toMinutes(queryDuration) + "min ago";
        if (queryDuration < THIRTY_HOURS)
            return TimeUnit.MILLISECONDS.toHours(queryDuration) + "hrs ago";
        return TimeUnit.MILLISECONDS.toDays(queryDuration) + " days ago";
    }

    public String getQueryDuration() {
        String duration = getDuration(xact_start);
        if (StringUtils.isBlank(duration)) {
            duration = getDuration(query_start) + " ago (transaction over)";
        }
        return duration;
    }

    public RunningQuery(String pid, String state, String query, Timestamp xact_start) {
        this.pid = pid;
        this.state = state;
        this.query = query;
        this.xact_start = xact_start;
    }

    public String getDatname() {
        return datname;
    }

    public RunningQuery setDatname(String datname) {
        this.datname = datname;
        return this;
    }

    public String getUsesysid() {
        return usesysid;
    }

    public RunningQuery setUsesysid(String usesysid) {
        this.usesysid = usesysid;
        return this;
    }

    public String getUsename() {
        return usename;
    }

    public RunningQuery setUsename(String usename) {
        this.usename = usename;
        return this;
    }

    public String getApplication_name() {
        return application_name;
    }

    public RunningQuery setApplication_name(String application_name) {
        this.application_name = application_name;
        return this;
    }

    public String getClient_addr() {
        return client_addr;
    }

    public RunningQuery setClient_addr(String client_addr) {
        this.client_addr = client_addr;
        return this;
    }

    public String getClient_hostname() {
        return client_hostname;
    }

    public RunningQuery setClient_hostname(String client_hostname) {
        this.client_hostname = client_hostname;
        return this;
    }

    public String getClient_port() {
        return client_port;
    }

    public RunningQuery setClient_port(String client_port) {
        this.client_port = client_port;
        return this;
    }

    public Timestamp getBackend_start() {
        return backend_start;
    }

    public RunningQuery setBackend_start(Timestamp backend_start) {
        this.backend_start = backend_start;
        return this;
    }

    public Timestamp getXact_start() {
        return xact_start;
    }

    public RunningQuery setXact_start(Timestamp xact_start) {
        this.xact_start = xact_start;
        return this;
    }

    public RunningQuery setState_change(Timestamp stateChange) {
        this.stateChange = stateChange;
        return this;
    }

    public Timestamp getStateChange() {
        return stateChange;
    }

    public RunningQuery setSysdate(Timestamp sysdate) {
        this.sysdate = sysdate;
        return this;
    }

    public Timestamp getSysdate() {
        return sysdate;
    }

    //</editor-fold>
}
