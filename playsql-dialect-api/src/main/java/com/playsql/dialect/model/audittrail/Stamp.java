package com.playsql.dialect.model.audittrail;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Calendar;

/** The timestamp of an audit-trailed object */
public class Stamp {
    public final String author;
    public final Calendar timestamp;
    public final String comments;
    public Stamp(String author, Calendar timestamp, String comments) {
        this.author = author;
        this.timestamp = timestamp;
        this.comments = comments;
    }
}
