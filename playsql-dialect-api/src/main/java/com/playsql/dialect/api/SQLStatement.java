package com.playsql.dialect.api;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.collect.Lists;
import org.springframework.jdbc.core.SqlParameterValue;
import org.springframework.jdbc.core.SqlTypeValue;
import org.springframework.jdbc.core.StatementCreatorUtils;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;


/**
 * Wrap a SQL statement and its arguments. Also give it a name, so it's easier to locate when an exception is thrown.
 **/
public class SQLStatement implements Serializable {
    private String name;
    private String sql;
    private List<Object> arguments = Lists.newArrayList();

    public SQLStatement(String sql) {
        this.sql = sql;
    }

    public SQLStatement(String sql, List<Object> args) {
        this.sql = sql;
        this.arguments = args;
    }

    public SQLStatement(String sql, Object... args) {
        this.sql = sql;
        this.arguments = Lists.newArrayList(args);
    }

    public final String sql() {
        return sql;
    }

    public void sql(String sql) {
        this.sql = sql;
    }

    public void setValues(PreparedStatement ps) throws SQLException
    {
        Object[] args = arguments.toArray();
        if (args != null)
        {
            for (int i = 0; i < args.length; i++)
            {
                Object arg = args[i];
                if (arg instanceof SqlParameterValue)
                {
                    SqlParameterValue paramValue = (SqlParameterValue) arg;
                    StatementCreatorUtils.setParameterValue(ps, i + 1, paramValue, paramValue.getValue());
                }
                else
                {
                    StatementCreatorUtils.setParameterValue(ps, i + 1, SqlTypeValue.TYPE_UNKNOWN, arg);
                }
            }
        }
    }

    public SQLStatement addArgument(Object... args) {
        if (args != null)
            for (Object arg : args)
                arguments.add(arg);
        return this;
    }
    public SQLStatement addBinaryArg(byte [] arg) {
        arguments.add(arg);
        return this;
    }
    public List<Object> getArguments() {
        return arguments;
    }

    public void setArguments(List<Object> arguments) {
        this.arguments = arguments;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        if (name != null)
            result.append("[name=").append(name).append("] ");
        result.append(sql);
        if (arguments != null)
            for (Object argument : arguments)
                result.append(", ").append(Objects.toString(argument, "null"));
        return result.toString();
    }
}