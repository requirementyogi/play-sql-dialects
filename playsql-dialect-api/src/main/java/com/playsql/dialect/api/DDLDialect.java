package com.playsql.dialect.api;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.collect.Multimap;
import com.playsql.spi.models.Tuple;
import org.springframework.jdbc.core.ResultSetExtractor;

import javax.annotation.Nullable;
import java.util.List;

/**
 * Main interface of a dialect. It extends {@link DDLDialectMetadata}, and a set of capabilities.
 */
public interface DDLDialect
        extends
                DDLDialectMetadata,
                CapabilitySchemaManagement,
                CapabilityDataManagement,
                CapabilityTableManagement,
                CapabilityAuditTrail,
                CapabilityFormulas,
                CapabilityMonitoring,
                CapabilityEntities,
                // Each time we open a connection, the generic DDLDialect
                // instance will be cloned and the JdbcWrapper will be provided
                // to this instance.
                Cloneable
{
    /*
     * == Note ==
     * Most of the methods have been moved to dedicated interfaces. What you see below are the miscellaneous
     * junk methods which couldn't be categorized.
     */


    enum DropBehaviour { RESTRICT, CASCADE }

    /**
     * /!\ If you want to escape the string as-is, please use quoteEntityName().
     * Modifies the suggested name to make it suitable for table names, column names and other entity names */
    String escapeEntityName(String name);

    /** Adds the necessary quotes/doublequotes/backticks around the name of a column/table, and escapes it */
    String quoteEntityName(String name);

    /** Adds quotes for an SQL string - "it's" becomes "'it''s'". */
    String quoteString(String string);

    String makeSchemaName(String spaceKey);

    /**
     * Generates an example of SQL for the user. Limit to 20.
     * @param table the SQL table name
     * @return the sql
     */
    String sqlSelectTable(String table);


    /**
     * Returns the session information
     */
    CurrentSessionInformation getCurrentUserAndSchema();

    /**
     * List all tables
     * @param schema if null, the current schema is assumed.
     * @param startingWith beginning of the table name - may be null
     * @return a list of { schema, table }
     */
    Multimap<String, String> listTables(String schema, boolean allSchemas, String startingWith, boolean withIntegerColumn);

    int countUpTo101(String tableName);

    /**
     * Returns the list of Play SQL tables. Are assumed Play SQL tables: Tables with a label. We shalt as well
     * test for the ID and POSITION column, but that's too much asking.
     * @return list of {key, label} of the spreadsheets
     */
    List<Tuple<String, String>> listPlaySqlTableNames(boolean includeDeleted);

    <T> T queryForTotals(List<String> fieldList, String sql, List<Object> arguments, ResultSetExtractor applier);


    @FunctionalInterface
    interface ReadDataFunction {
        String readData(String fieldlist,
                           Long limit,
                           Long offset,
                           String order,
                           String tablename,
                           String whereClause);
    }

    ReadDataFunction getStatementForReadData();

    /** Creates the settings table. Only used on write connections. */
    void createSettingsTable();
    String getSettings(String category, String key);
    void setSettings(String category, String key, String value);


    List<String> getColumnList(String tableName);

    DDLDialect clone(Jdbc jdbc);

    Jdbc getJdbc();

    void purgeCache(@Nullable String tableName);
}