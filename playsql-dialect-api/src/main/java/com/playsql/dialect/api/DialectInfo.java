package com.playsql.dialect.api;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * This annotation provides a bit of information about the dialect, in a way which could for example
 * be sent to the front-end safely. Especially, it is serializable and doesn't include binary objects.
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE})
public @interface DialectInfo {

    /** The key of the dialect. It used to be the package name, until we wanted to be
     * flexible about that.
     */
    public String key();

    /**
     * Internationalization key for the dialect name
     */
    public String i18n();

    /* Capabilities */
    public boolean readWrite() default false;
    public boolean autocomplete() default false;
    public boolean monitoring() default false;
    public DDLDialect.CaseSensibility defaultCase();

    /* Metadata for the UI */

    /** Text to display to the user when they choose the dialect. */
    public String titleText() default "";

    /** Link to display to the user when they choose the dialect. */
    public String titleLink() default "";

    /** The default name of the driver. Displayed by default in the UI, the user
     * may choose to change it.
     *
     * The value is for user information only. It has no incidence on the
     * behaviour of the dialect.
     * */
    public String driverName();

    /** An example of URL to this driver, so the user doesn't go to
     * the interwebs to build their jdbc connection url.
     *
     * The value is for user information only. It has no incidence on the
     * behaviour of the dialect.
     * */
    public String exampleUrl();

    /** When changing dialects, it will override the user's entry
     * except if the {@link #minimumDriverString} is in it.
     *
     * The value is for user information only. It has no incidence on the
     * behaviour of the dialect.
     * */
    public String minimumDriverString() default "";

    /** When changing dialects, it will override the user's entry
     * except if the {@link #minimumUrlString} is in it.
     *
     * The value is for user information only. It has no incidence on the
     * behaviour of the dialect.
     * */
    public String minimumUrlString() default "always replace";

}
