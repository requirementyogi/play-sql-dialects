package com.playsql.dialect.api;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.sql.Driver;
import java.sql.DriverManager;

/**
 * Metadata about the dialect. All methods which don't interact with {@link Jdbc} should be here.
 */
public interface DDLDialectMetadata {

    String getDialectKey();
    String getDialectI18nName();

    CaseSensibility getDefaultCase();

    boolean isCapabilityReadwrite();
    boolean isCapabilityAutocomplete();
    boolean isCapabilityMonitoring();

    /** Return true if this dialect is an instance of 'dialect' */
    boolean is(String dialect);

    enum CaseSensibility { LOWER, UPPER }

    String getMinimumUrlString();
    String getMinimumDriverString();
    String getExampleUrl();
    String getDriverName();
    String getTitleLink();
    String getTitleText();

    /**
     * <p>Returns the driver for this dialect.</p>
     *
     * <p>This method is performed by the dialect because the dialect may belong to the
     * classloader of another plugin.</p>
     *
     * <p>This method is called at each request, so please don't return a new driver each time
     * or you'll create a memory leak.
     * The best situation is when drivers are singletons in the system (This is required of
     * Oracle drivers, for example). The Java-certified way is to call {@link DriverManager#getDrivers},
     * but OSGi doesn't respect Java's requirement of top-down classloaders, so the DriverManager
     * may not work.</p>
     *
     * <p>To check the classloading, please test:</p>
     * <ul>
     *     <li>A driver provided through JNDI</li>
     *     <li>A driver provided in &lt;confluence-install&gt;/lib</li>
     *     <li>A driver provided in the plugin</li>
     *     <li>If you're overloading an existing dialect which provides its driver, please check
     *         it works if the dialect and the driver are in different plugins</li>
     * </ul>
     *
     * @param canonicalName It should be the name of the driver, unless the user has overridden it
     *                      with a custom name. Please return the driver the user requested, since
     *                      they may have provided a new version or something.
     * @return the driver instance. Must be a singleton in the system.
     */
    Driver getDriverInstance(String canonicalName);
}
