package com.playsql.dialect.api;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.dialect.exceptions.JdbcException;
import com.playsql.spi.models.Tuple;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.jdbc.support.JdbcUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.*;

import static java.lang.String.format;
import static java.sql.Statement.RETURN_GENERATED_KEYS;

/**
 * Wrapper for all basic JDBC operations: Open a connection, fetch one row, fetch all rows as objects,
 * close silently, query with a timeout...
 */
public class Jdbc
{
    final static Logger log = LoggerFactory.getLogger(Jdbc.class.getCanonicalName());
    /** The mode for opening the connection. DO NOT USE for the mode of the schema. Only READ_WRITE schemas can use READ_WRITE cnx. */
    public enum ConnectionMode { CNX_RO, CNX_RW }

    final private Connection cnx;
    final private ExecutorService executor;
    final private ConnectionMode cnxMode;
    final private String dsIdentifier;
    final private UserPassword dbCreds;
    private boolean open = true;

    public static Jdbc openConnection(DataSource ds, String dsIdentifier, ConnectionMode cnxMode, UserPassword dbCreds, ExecutorService executor)
    {
        Connection connection;
        try {
            if (dbCreds != null)
                connection = ds.getConnection(dbCreds.getDbUser(), dbCreds.getDbPassword());
            else
                connection = ds.getConnection();

            if (connection == null)
                throw new RuntimeException("JDBC didn't return a connection upon request. Maybe check the connection url for the right driver name.");
            if (cnxMode == ConnectionMode.CNX_RW) {
                connection.setAutoCommit(false);
                connection.setReadOnly(false);
            } else {
                if (!Boolean.getBoolean("play.sql.read.only.complacent")) {
                    connection.setReadOnly(true);
                }
            }
        } catch (SQLException e) {
            throw new JdbcException("Couldn't open the connection", null, e);
        }
        return new Jdbc(connection, dsIdentifier, cnxMode, executor, dbCreds);
    }

    public static class UserPassword {
        final private String dbUser;
        final private String dbPassword;

        public UserPassword(String dbUser, String dbPassword) {
            this.dbUser = dbUser;
            this.dbPassword = dbPassword;
        }
        public String getDbPassword() {
            return dbPassword;
        }
        public String getDbUser() {
            return dbUser;
        }

        public static UserPassword convert(String dbUser, String dbPassword) {
            if (dbUser == null) return null;
            return new UserPassword(dbUser, dbPassword);
        }

        /** Two objects are equal if their user and pw are equal.
         * This condition is necessary when reusing an existing connection, to check we're using the same creds.
         */
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            UserPassword that = (UserPassword) o;

            if (dbUser != null ? !dbUser.equals(that.dbUser) : that.dbUser != null) return false;
            return !(dbPassword != null ? !dbPassword.equals(that.dbPassword) : that.dbPassword != null);

        }

        @Override
        public int hashCode() {
            return dbUser != null ? dbUser.hashCode() : 0;
        }

        @Override
        public String toString() {
            return String.format("User %s/%s",
                    dbUser,
                    StringUtils.isBlank(dbPassword) ? "[blank]" : "[hidden]");
        }
    }

    public static Jdbc createForTestPurpose(Connection cnx, String dsIdentifier, ConnectionMode cnxMode, ExecutorService executor, UserPassword dbCreds) {
        return new Jdbc(cnx , dsIdentifier, cnxMode, executor, dbCreds);
    }

    private Jdbc(Connection cnx, String dsIdentifier, ConnectionMode cnxMode, ExecutorService executor, UserPassword dbCreds)
    {
        this.cnx = cnx;
        this.dsIdentifier = dsIdentifier;
        this.executor = executor;
        this.dbCreds = dbCreds;
        this.open = true;
        this.cnxMode = cnxMode;
    }
    
    public void commit()
    {
        if (cnxMode == ConnectionMode.CNX_RW) {
            try
            {
                cnx.commit();
            }
            catch (SQLException e)
            {
                throw new JdbcException("Couldn't commit the transaction", null, e);
            }
        }
    }

    public void rollback()
    {
        if (cnxMode == ConnectionMode.CNX_RW) {
            try
            {
                cnx.rollback();
            }
            catch (SQLException e)
            {
                throw new JdbcException("Couldn't rollback the transaction", null, e);
            }
        }
    }

    public boolean isOpen()
    {
        return open;
    }

    public Connection cnx() {
        return cnx;
    }

    public void closeSilently()
    {
        try
        {
            cnx.close();
        }
        catch (SQLException sqle)
        {
            log.info("Error while closing the connexion");
        }
        open = false;
    }

    public <T> T queryForOne(SQLStatement sql, Class<T> resultType)
    {
        if (Tuple.class.isAssignableFrom(resultType))
            return queryForOne(sql, new RowMapper(){
                @Override
                public Tuple<String, String> mapRow(ResultSet rs, int rowNum) throws SQLException {
                    return new Tuple (rs.getString(1), rs.getString(2));
                }
            });
        else
            return queryForOne(sql, new SingleColumnRowMapper(resultType));
    }

    public <T> T queryForOne(SQLStatement sql, RowMapper rowMapper) {
        List<T> results = query(sql, rowMapper);
        if (results.isEmpty()) return null;
        return results.get(0);
    }

    public <T> T queryForOne(SQLStatement sql, ResultSetExtractor resultSetExtractor) {
        List<T> results = query(sql, resultSetExtractor);
        if (results.isEmpty()) return null;
        return results.get(0);
    }

    public <T> T queryForOne(String sql, Class<T> resultType)
    {
        return queryForOne(sql, null, resultType);
    }

    public <T> T queryForOne(String sql, Object[] args, Class<T> resultType)
    {
        if (Tuple.class.isAssignableFrom(resultType))
            return queryForOne(sql, args, new RowMapper(){
                @Override
                public Tuple<String, String> mapRow(ResultSet rs, int rowNum) throws SQLException {
                    return new Tuple (rs.getString(1), rs.getString(2));
                }
            });
        else
            return queryForOne(sql, args, new SingleColumnRowMapper(resultType));
    }

    public <T> T queryForOne(String sql, RowMapper rowMapper)
    {
        return queryForOne(sql, null, rowMapper);
    }

    public <T> T queryForOne(String sql, Object[] args, RowMapper rowMapper)
    {
        List<T> results = query(sql, args, rowMapper);
        if (results.isEmpty()) return null;
        return results.get(0);
    }

    public <T> List<T> query(String sql, Class<T> resultType) {
        return query(sql, null, resultType);
    }

    /**
     * @param resultType a primitive type of a Tuple<String, String, String>
     */
    public <T> List<T> query(String sql, Object[] args, Class<T> resultType)
    {
        if (Tuple.class.isAssignableFrom(resultType))
            return query(sql, args, (RowMapper) (rs, rowNum) -> new Tuple (rs.getString(1), rs.getString(2)));

        else if (String[].class.isAssignableFrom(resultType))
            return query(sql, args, (RowMapper) (rs, rowNum) -> new String[] { rs.getString(1), rs.getString(2) });

        else
            return query(sql, args, new SingleColumnRowMapper(resultType));
    }

    public <T> List<T> query(SQLStatement sql, RowMapper rowMapper)
    {
        return query(sql, new RowMapperResultSetExtractor(rowMapper));
    }

    public <T> List<T> query(SQLStatement sql, Class<T> resultType) {
        if (Tuple.class.isAssignableFrom(resultType))
            return query(sql, new RowMapper(){
                @Override
                public Tuple<String, String> mapRow(ResultSet rs, int rowNum) throws SQLException {
                    return new Tuple (rs.getString(1), rs.getString(2));
                }
            });
        else
            return query(sql, new SingleColumnRowMapper(resultType));

    }

    public <T> List<T> query(String sql, RowMapper rowMapper)
    {
        return query(sql, null, rowMapper);
    }

    public <T> List<T> query(String sql, final Object[] args, RowMapper rowMapper)
    {
        return query(new SQLStatement(sql).addArgument(args), new RowMapperResultSetExtractor(rowMapper));
    }

    public <T> List<T> query(String sql, ResultSetExtractor resultSetExtractor)
    {
        return query(new SQLStatement(sql), resultSetExtractor);
    }

    public <T> List<T> query(final SQLStatement sql, final ResultSetExtractor rse, final Long timeout) {
        Future<List<T>> future = executor.submit(new Callable<List<T>>() {
            @Override
            public List<T> call() {
                return query(sql, rse);
            }
        });

        try {
            if (timeout == null) {
                return future.get();
            } else {
                return future.get(timeout, TimeUnit.MILLISECONDS);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.info("The thread was interrupted while waiting for the Play SQL answer. The current HTTP request is being terminated.", e);
            throw new RuntimeException("The thread was interrupted while waiting for the Play SQL answer. The current HTTP request is being terminated.", e);
        } catch (ExecutionException e) {
            if (e.getCause() instanceof RuntimeException) {
                log.debug("Callable<T> returned a runtime exception: rethrowing", e);
                throw (RuntimeException) e.getCause();
            }
            log.info("Callable<T> returned an unmanaged checked exception", e);
            throw new IllegalStateException("Callable<T> returned an unmanaged checked exception", e.getCause());
        } catch (java.util.concurrent.TimeoutException e) {
            log.debug("Timeout exception for the query", e);
            throw new com.playsql.dialect.exceptions.TimeoutException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> query(final SQLStatement sql, final ResultSetExtractor rse)
    {
        return (List<T>) prepareStatement(sql, new StatementCallback() {
            @Override
            public Object doInPreparedStatement(PreparedStatement ps) throws SQLException {
                ResultSet rs = null;
                try {
                    if (Thread.interrupted()) return null;
                    rs = ps.executeQuery();
                    if (Thread.interrupted()) return null;
                    List<T> results = (List<T>) rse.extractData(rs);
                    if (results != null) log.debug(results.size() + " rows returned");
                    else log.debug("No row returned");
                    return results;
                } finally {
                    JdbcUtils.closeResultSet(rs);
                }
            }
        }, false);

    }

    public Integer update(String updateSql)
    {
        return update(updateSql, null);
    }
    public Integer update(String updateSql, Object... args)
    {
        final SQLStatement sql = new SQLStatement(updateSql);
        if (args != null) sql.addArgument(args);
        return update(sql);
    }
    public Integer update(boolean isBinary, String updateSql, byte[] args)
    {
        final SQLStatement sql = new SQLStatement(updateSql);
        if (args != null) sql.addBinaryArg(args);
        return update(sql);
    }
    public Integer update(final SQLStatement sql) {
        return (Integer) prepareStatement(sql, new StatementCallback() {
            @Override
            public Object doInPreparedStatement(PreparedStatement ps) throws SQLException {
                int count = ps.executeUpdate();
                return Integer.valueOf(count);
            }
        }, false);
    }
    public Integer insert(final SQLStatement sql) {
        return (Integer) prepareStatement(sql, new StatementCallback() {
            @Override
            public Integer doInPreparedStatement(PreparedStatement ps) throws SQLException {
                ResultSet rs = null;
                try {
                    int count = ps.executeUpdate();
                    rs = cnx.prepareStatement("CALL IDENTITY();").executeQuery();
                    if (rs.next())
                        return rs.getInt(1);

                    throw new JdbcException(format("INSERT returns %d, " +
                            "but there are no generated keys",
                            count), sql.toString(), null);
                } finally {
                    JdbcUtils.closeResultSet(rs);
                }
            }
        }, true);
    }

    public void insert2(final SQLStatement sql) {
        prepareStatement(sql, new StatementCallback() {
            @Override
            public Void doInPreparedStatement(PreparedStatement ps) throws SQLException {
                ps.executeUpdate();
                return null;
            }
        }, true);
    }

    public Integer insertWithGeneratedKeys(final SQLStatement sql) {
        return (Integer) prepareStatement(sql, new StatementCallback() {
            @Override
            public Integer doInPreparedStatement(PreparedStatement ps) throws SQLException {
                ResultSet rs = null;
                try {
                    int count = ps.executeUpdate();
                    rs = ps.getGeneratedKeys();
                    if (rs.next()) return rs.getInt(1);
                    throw new JdbcException(format("INSERT returns %d rows, " +
                            "there are no generated keys",
                            count), sql.toString(), null);
                } finally {
                    JdbcUtils.closeResultSet(rs);
                }
            }
        }, true);
    }

    public void execute(String sql)
    {
        try
        {
            if (dbCreds != null) {
                log.debug("Executing SQL as {}:\n{}", dbCreds.getDbUser(), sql);
            } else {
                log.debug("Executing SQL:\n{}", sql);
            }
            cnx.createStatement().execute(sql);
        }
        catch (SQLException e)
        {
            throw new JdbcException(null, sql, e);
        }
    }
    
    private Object prepareStatement(SQLStatement sqlStatement, StatementCallback statementCallback, boolean withGeneratedKeys)
    {
        String sql = sqlStatement.sql();
        try
        {
            if (dbCreds != null) {
                log.debug("Executing SQL as {}:\n{}", dbCreds.getDbUser(), sqlStatement);
            } else {
                log.debug("Executing SQL:\n{}", sqlStatement);
            }
            PreparedStatement ps;
            try {
                ps = withGeneratedKeys
                    ? cnx.prepareStatement(sql, RETURN_GENERATED_KEYS)
                    : cnx.prepareStatement(sql);
            } catch (RuntimeException ex) {
                throw new JdbcException("Exception while preparing the query", sql, ex);
            }
            applyStatementSettings(ps);
            sqlStatement.setValues(ps);
            return statementCallback.doInPreparedStatement(ps);
        }
        catch (SQLException sqle)
        {
            throw new JdbcException(null, sql, sqle);
        }
    }

    private void applyStatementSettings(PreparedStatement ps)
    {
        // TODO Use when appropriate
    }



    private static abstract class StatementCallback
    {
        public abstract Object doInPreparedStatement(PreparedStatement ps) throws SQLException;
    }

    public String getDatasourceIdentifier() {
        return dsIdentifier;
    }
}
