package com.playsql.dialect.api;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.dialect.model.audittrail.AuditTrailItem;

import java.util.List;

public interface CapabilityAuditTrail {
    void createAuditTrailTable();

    void saveAuditTrailItem(AuditTrailItem item);

    List<AuditTrailItem> listAuditTrail(String tableName, String columnName, Integer rowId, String author, int limit, int offset);

    List<AuditTrailItem> auditTrailTrimByDays(Integer duration);

    List<AuditTrailItem> auditTrailTrimByCount(Integer count);
}
