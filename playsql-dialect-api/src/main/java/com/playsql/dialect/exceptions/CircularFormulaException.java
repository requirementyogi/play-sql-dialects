package com.playsql.dialect.exceptions;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.dialect.model.formulas.Endpoint;

/**
 * When a user parameter has no value
 */
public class CircularFormulaException extends FlowControlException {

    private Endpoint endpoint;

    public CircularFormulaException(Endpoint endpoint, String loopPath) {
        super("This formula references itself: " + loopPath);
                /*+ endpoint.getTableName() + "."
                + (endpoint.getColumnName() != null ? endpoint.getColumnName() : "*")
                + (endpoint.getRowId() != null ? ":" + endpoint.getRowId() : ":*")
                + (endpoint.getFormula() != null ? endpoint.getFormula() : "=(null)"));*/
        this.endpoint = endpoint;
    }

    public Endpoint getEndpoint() {
        return endpoint;
    }
}