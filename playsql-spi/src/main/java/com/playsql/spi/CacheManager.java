package com.playsql.spi;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.base.Function;

import java.io.Serializable;

public interface CacheManager {

    <T extends Serializable> Cache<T> getCache(String cacheName, Class<T> clazz);

    /**
     * Returns a cache
     * @param cacheName the name of the cache
     * @param clazz the type of the cache, for Reflection verifications. Null if no verification should be done,
     *              although for Data Center purpose it must be {@code Serializable}.
     * @param hasNullValues if true, then the null values are cached
     * @param cloner if available, the cached values are immutable and only clones are returned
     * @param <T> the type of the cache, for static compilation sake
     * @return the value
     */
    <T extends Serializable> Cache<T> getCache(String cacheName, Class<T> clazz, boolean hasNullValues, Function<T, T> cloner);

    interface Cache<T extends Serializable> {
        T get(String key);
        T get(String key, T defaultValue);
        T getOrSet(String key, InitialValue<? extends T> initializer);

        /**
         * Clear the cache for the current instance (there is only 1 for Confluence Server)
         */
        boolean removeAll();

        /**
         * Clear the cache for all instances (equivalent to {@link #removeAll()} for Confluence Server)
         */
        boolean removeAllAll();

        /**
         * Remove 1 key for the current, logged-in instance
         */
        boolean remove(String key);
    }

    interface InitialValue<V> {
        V generate();
    }

    /**
     * For all caches created by this CacheManager, clears the cache for the current clientKey.
     * Only in PCloud. Noop in Confluence Server.
     */
    void removeAll();
}
