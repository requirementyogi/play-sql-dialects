package com.playsql.spi.utils;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.concurrent.TimeUnit;

public class Timeout {

    private long endTime;

    private Timeout(long duration) {
        // Prevent long overflow
        if (duration == Long.MAX_VALUE) {
            this.endTime = Long.MAX_VALUE;
        } else {
            this.endTime = System.nanoTime() + duration;
        }
    }

    /**
     * Tells whether the time is out
     * @return true if the time is over, false until then
     */
    public boolean lapsed() {
        return System.nanoTime() > endTime;
    }

    /**
     * Cancels the timeout, so lapsed() will never return {@code true}.
     * @return this
     */
    public Timeout cancel() {
        endTime = Long.MAX_VALUE;
        return this;
    }

    public Timeout reset(long durationInMillis) {
        endTime = TimeUnit.MILLISECONDS.toNanos(durationInMillis);
        return this;
    }

    public static Timeout millis(long duration) {
        return new Timeout(TimeUnit.MILLISECONDS.toNanos(duration));
    }

    public static Timeout forever() {
        return new Timeout(Long.MAX_VALUE);
    }

    @Override
    public String toString() {
        return String.format("Timeout{%d ms left}",
                TimeUnit.NANOSECONDS.toMillis(endTime - System.nanoTime()));
    }
}
