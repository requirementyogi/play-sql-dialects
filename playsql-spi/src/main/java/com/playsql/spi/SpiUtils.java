package com.playsql.spi;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

/**
 * Various methods which are implemented by the host application.
 * <p/>
 * SPI stands for Service Provider Interface, it's a bit like an API but it mostly comes
 * from a technical requirement of separation of concerns, and doesn't deserve the name API.
 * <p>Here are the dependencies:</p>
 * <ul>
 *     <li>Dialect plugin shouldn't be aware of Play SQL Base</li>
 *     <li>Dialect plugin uses SPI</li>
 *     <li>Play SQL Base implements SPI</li>
 * </ul>
 */
public interface SpiUtils {

    <T> Class<T> loadClass(final String className, final Class<?> callingClass) throws ClassNotFoundException;
    String htmlEncode(String htmlEncode);
    String urlEncode(String part);

    String getConfluenceHome();

    String getI18n(String message, Object... arguments);
    /** Return the same as getI18n, but is not html-encoded by Velocity */
    String getI18nHtml(String message, Object... arguments);

    final class SpiUser {
        public final String key;
        public final String username;
        public final String fullName;
        public final String email;

        public SpiUser(String key, String username, String fullName, String email) {
            this.email = email;
            this.key = key;
            this.username = username;
            this.fullName = fullName;
        }

        public String getEmail() {
            return email;
        }

        public String getFullName() {
            return fullName;
        }

        public String getKey() {
            return key;
        }

        public String getUsername() {
            return username;
        }
    }

    SpiUser getUserByUsername(String username);
    SpiUser getUserByKey(String username);
    SpiUser getLoggedInUser();
    String getClientKey();
    String getLoggedInUserName();
    String getGroup(String groupname);

    void setAlternativeAuthentication(String user, String clientKey);
}
