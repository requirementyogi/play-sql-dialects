---
-- #%L
-- Play SQL Dialects
-- %%
-- Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
-- %%
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--      http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- #L%
---

CREATE SCHEMA space_readonly;

SET search_path = space_readonly, pg_catalog;

CREATE TABLE products (
    "ID" integer NOT NULL,
    "POSITION" integer,
    name character varying(50),
    secondcolumn character varying(50),
    price numeric,
    startdate date,
    sales integer,
    "_DELETED_started" integer,
    "_DELETED_other1" text,
    "_DELETED_other2" text,
    "_DELETED_other3" text,
    "_DELETED_other4" text,
    "_DELETED_other5" text,
    "_DELETED_other6" text,
    "_DELETED_other7" text
);

CREATE SEQUENCE "products_ID_seq1"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE "products_ID_seq1" OWNED BY products."ID";

ALTER TABLE ONLY products ALTER COLUMN "ID" SET DEFAULT nextval('"products_ID_seq1"'::regclass);

INSERT INTO products ("ID", "POSITION", name, secondcolumn, price, startdate, sales, "_DELETED_started", "_DELETED_other1", "_DELETED_other2", "_DELETED_other3", "_DELETED_other4", "_DELETED_other5", "_DELETED_other6", "_DELETED_other7") VALUES(7, 7, 'Plates', 'Paul Levis', 91, '2014-12-28', 432, null, null, null, null, null, null, null, null);
