package com.playsql.jdbc.dialect.templates

class TemplatesForOracle : TemplatesForGeneric() {

    override fun selectTable(table: String): String {
        val query = """
            SELECT * FROM $table WHERE ROWNUM < 20
        """
        return query
    }


    override fun readData(fieldlist: String,
                          limit: Long?,
                          offset: Long?,
                          order: String?,
                          tablename: String,
                          whereClause: String?): String {
        val query = """
            SELECT $fieldlist
            FROM $tablename TARGET_TABLE
            ${where(whereClause)}
            ${if (limit != null) "WHERE ROWNUM < $limit" else ""}
            ${order(order)}
        """
        return query
    }

}