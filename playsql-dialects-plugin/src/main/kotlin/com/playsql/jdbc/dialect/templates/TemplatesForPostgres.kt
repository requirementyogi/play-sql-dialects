package com.playsql.jdbc.dialect.templates

class TemplatesForPostgres : TemplatesForGeneric() {

    fun using(value: Any?, default: String = "") = helper("USING", value, default)

    override fun listTablesWithoutSchema(startingWith: Boolean,
                                         withIntegerColumn: Boolean,
                                         withCurrentSchema: Boolean): String {
        val query = """
            select TABLE_SCHEMA, TABLE_NAME
            from INFORMATION_SCHEMA.TABLES t
            where
            ${if (withCurrentSchema)
                "UPPER(TABLE_SCHEMA)=UPPER(CURRENT_SCHEMA())"
            else
                " UPPER(TABLE_SCHEMA) NOT IN ('INFORMATION_SCHEMA', 'PG_CATALOG') "
            }
            ${if (startingWith) "and UPPER(TABLE_NAME) LIKE (?)" else ""}
            ${if (withIntegerColumn) """
                and exists(
                  select * from information_schema.columns c
                  where c.table_catalog = t.table_catalog
                    and c.table_schema = t.table_schema
                    and c.table_name = t.table_name
                    and data_type in ('integer', 'bigint', 'smallint', 'numeric')
                )
            """ else ""}
        """
        return query
    }


    override fun listTablesWithSchema(startingWith: Boolean,
                                      withIntegerColumn: Boolean): String {
        val query = """
            select T.TABLE_SCHEMA, TABLE_NAME
            from INFORMATION_SCHEMA.TABLES t
            where UPPER(TABLE_SCHEMA)=UPPER(?)
            ${if (startingWith) "and UPPER(TABLE_NAME) LIKE (?)" else ""}
            ${if (withIntegerColumn) """
                and exists(
                  select * from information_schema.columns c
                  where c.table_catalog = t.table_catalog
                    and c.table_schema = t.table_schema
                    and c.table_name = t.table_name
                    and data_type in ('integer', 'bigint', 'smallint', 'numeric')
                )
            """ else ""}
        """
        return query
    }


    override fun listTablesAndComments(): String {
        val query = """
            SELECT
            CAST( CASE c.relkind
              WHEN 'r' THEN 'table'
              WHEN 'v' THEN 'view'
              WHEN 'f' THEN 'foreign table'
              END AS pg_catalog.text) as objtype,
            c.oid as objectid,
            c.tableoid as tableoid,
            n.nspname as namespace,
            CAST(c.relname AS pg_catalog.text) as tablename,
            d.description com

            FROM pg_catalog.pg_class c
            LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
            LEFT JOIN pg_catalog.pg_description d ON (c.oid = d.objoid AND c.tableoid = d.classoid AND d.objsubid = 0)

            WHERE c.relkind IN ('r', 'v', 'f')
            AND n.nspname <> 'pg_catalog'
            AND n.nspname <> 'information_schema'
            AND pg_catalog.pg_table_is_visible(c.oid)
            AND n.nspname = CURRENT_SCHEMA()
        """
        return query
    }


    override fun currentUserAndSchema(): String {
        val query = """
            SELECT CURRENT_USER, CURRENT_SCHEMA
        """
        return query
    }


    override fun hasSchema(): String {
        val query = """
            SELECT COUNT(*) FROM information_schema.schemata
            WHERE schema_name = ?
        """
        return query
    }


    override fun getCommentOnTable(): String {
        val query = """
            SELECT table_name, obj_description(('"' || table_name || '"')::regclass, 'pg_class') COM
            FROM information_schema.tables
            WHERE table_schema=CURRENT_SCHEMA()
            AND table_name = ?
        """
        return query
    }


    override fun getCommentOnColumn(): String {
        val query = """
            SELECT d.description as COMMENT
            FROM pg_description d
            JOIN pg_attribute a ON d.objsubid = a.attnum AND d.objoid = a.attrelid
            WHERE a.attrelid = ('"' || ? || '"')::regclass
            AND pg_table_is_visible(a.attrelid)
            AND a.attname = ?
        """
        return query
    }


    override fun listColumns(): String {
        val query = """
            select
                T.COLUMN_NAME,
                T.DATA_TYPE,
                col_description(('"' || T.TABLE_NAME || '"')::regclass, T.ORDINAL_POSITION) COM,
                T.CHARACTER_MAXIMUM_LENGTH
            from INFORMATION_SCHEMA.COLUMNS T
            where T.TABLE_NAME = ?
            and T.TABLE_SCHEMA = CURRENT_SCHEMA()
            order by T.ORDINAL_POSITION
        """
        return query
    }


    override fun dropTable(dropBehaviour: String,
                  ifExists: Boolean,
                  table: String): String {
        val query = """
            DROP TABLE ${if (ifExists) "IF EXISTS" else ""} "$table" $dropBehaviour
        """
        return query
    }


    override fun count101(tableName: String,
                          whereClause: String?): String {
        val query = """
            SELECT COUNT(*) FROM (
            SELECT 1 FROM $tableName
                ${where(whereClause)}
            LIMIT 101
            ) SUBTABLE
        """
        return query
    }

    fun alterColumn(column: String,
                    newType: String,
                    table: String,
                    using: String?): String {
        val query = """
            ALTER TABLE $table
            ALTER COLUMN $column TYPE $newType
            ${using(using)}
        """
        return query
    }


    override fun addColumn(beforeColumn: String?,
                           columnDefinition: String,
                           table: String): String {
        val query = """
            ALTER TABLE $table
            ADD COLUMN $columnDefinition
        """
        return query
    }


    override fun readData(fieldlist: String,
                          limit: Long?,
                          offset: Long?,
                          order: String?,
                          tablename: String,
                          whereClause: String?): String {
        val query = """
              SELECT $fieldlist
              FROM $tablename TARGET_TABLE
              ${where(whereClause)}
              ${order(order)}
              ${limit(limit)}
              ${offset(offset)}
        """
        return query
    }


    override fun queryTotals(fieldlist: List<String>,
                             subquery: String?,
                             tablename: String?): String {
        val query = """
            SELECT ${fieldlist.joinToString()}
            FROM ${if (subquery != null) "( $subquery ) INTERNAL_RESULTS" else "$tablename"}
            LIMIT 1
        """
        return query
    }


    override fun getLastGeneratedId(): String {
        val query = """
            select LASTVAL()
        """
        return query
    }

    fun monitoringActivity(pid: Long?): String {
        val query = """
            SELECT pid, state, query, query_start, backend_start, xact_start, state_change,
                   application_name, client_addr, client_hostname, client_port, datname, usename, usesysid,
                   CASE state WHEN 'active' THEN 1
                              WHEN 'idle in transaction' THEN 2
                              WHEN 'idle' THEN 3
                              ELSE 0
                   END orderby,
                   NOW() sysdate
            FROM pg_stat_activity
            WHERE query_start < (current_timestamp - interval '1 second')
            ${if (pid != null) "AND pid = $pid" else ""}
            ORDER BY orderby ASC, query_start DESC
        """
        return query
    }

    fun monitoringActivityKill(pid: Long): String {
        val query = """
            SELECT pg_catalog.pg_terminate_backend($pid)
        """
        return query
    }

    fun monitoringLocks(): String {
        val query = """
            SELECT relation::REGCLASS, locktype, database, relation, page, tuple, virtualxid, transactionid,
                classid, objid, objsubid, virtualtransaction, pid, mode, granted, fastpath
            FROM pg_locks
        """
        return query
    }

    fun monitoringCursors(): String {
        val query = """
            SELECT name, statement, is_holdable, is_binary, is_scrollable, creation_time
            FROM pg_cursors
        """
        return query
    }

    fun monitoringBlocking(): String {
        val query = """
            SELECT bl.pid                 AS blocked_pid,
                a.usename              AS blocked_user,
                ka.query               AS blocking_statement,
                now() - ka.query_start AS blocking_duration,
                kl.pid                 AS blocking_pid,
                ka.usename             AS blocking_user,
                a.query                AS blocked_statement,
                now() - a.query_start  AS blocked_duration
            FROM  pg_catalog.pg_locks         bl
            JOIN pg_catalog.pg_stat_activity a  ON a.pid = bl.pid
            JOIN pg_catalog.pg_locks         kl ON kl.transactionid = bl.transactionid AND kl.pid != bl.pid
            JOIN pg_catalog.pg_stat_activity ka ON ka.pid = kl.pid
            WHERE NOT bl.granted
        """
        return query
    }


}