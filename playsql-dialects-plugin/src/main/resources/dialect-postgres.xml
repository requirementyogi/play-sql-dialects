<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  Play SQL Dialects
  %%
  Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  -->


<dialect name="postgres">

  <query name="list-tables-without-schema">
    <template engine="velocity">
      select TABLE_NAME
      from INFORMATION_SCHEMA.TABLES t
      where UPPER(TABLE_SCHEMA)=UPPER(CURRENT_SCHEMA())
      #if ($startingWith)
        and UPPER(TABLE_NAME) LIKE (?)
      #end
      #if ($withIntegerColumn)
        and exists(
            select * from information_schema.columns c
            where c.table_catalog = t.table_catalog
              and c.table_schema = t.table_schema
              and c.table_name = t.table_name
              and data_type in ('integer', 'bigint', 'smallint', 'numeric')
        )
      #end
    </template>
  </query>
  <query name="list-tables-with-schema">
    <template engine="velocity">
      select TABLE_NAME
      from INFORMATION_SCHEMA.TABLES t
      where UPPER(TABLE_SCHEMA)=UPPER(?)
      #if ($startingWith)
      and UPPER(TABLE_NAME) LIKE (?)
      #end
      #if ($withIntegerColumn)
        and exists(
            select * from information_schema.columns c
            where c.table_catalog = t.table_catalog
              and c.table_schema = t.table_schema
              and c.table_name = t.table_name
              and data_type in ('integer', 'bigint', 'smallint', 'numeric')
        )
      #end
    </template>
  </query>

  <query name="list-tables-and-comments">
    <template engine="velocity">SELECT
      CAST( CASE c.relkind
        WHEN 'r' THEN 'table'
        WHEN 'v' THEN 'view'
        WHEN 'f' THEN 'foreign table'
        END AS pg_catalog.text) as objtype,
      c.oid as objectid,
      c.tableoid as tableoid,
      n.nspname as namespace,
      CAST(c.relname AS pg_catalog.text) as tablename,
      d.description com

      FROM pg_catalog.pg_class c
      LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
      LEFT JOIN pg_catalog.pg_description d ON (c.oid = d.objoid AND c.tableoid = d.classoid AND d.objsubid = 0)

      WHERE c.relkind IN ('r', 'v', 'f')
      AND n.nspname &lt;&gt; 'pg_catalog'
      AND n.nspname &lt;&gt; 'information_schema'
      AND pg_catalog.pg_table_is_visible(c.oid)
      AND n.nspname = CURRENT_SCHEMA()
    </template>
  </query>

  <query name="current-user-and-schema">
    <template engine="velocity">
      SELECT CURRENT_USER, CURRENT_SCHEMA
    </template>
  </query>

  <query name="has-schema">
    <template engine="velocity">
      SELECT COUNT(*) FROM information_schema.schemata
      WHERE schema_name = ?
    </template>
  </query>

  <query name="get-comment-on-table">
    <template engine="velocity">
      SELECT table_name, obj_description(('"' || table_name || '"')::regclass, 'pg_class') COM
      FROM information_schema.tables
      WHERE table_schema=CURRENT_SCHEMA()
        AND table_name = ?
    </template>
  </query>

  <query name="get-comment-on-column">
    <!-- That'd be much finer if SELECT col_description('...table'::regclass, 0) didn't take the colnum as an arg -->
    <template engine="velocity">
      SELECT
      --    a.attname as name,
      pg_description.description as COMMENT
      FROM pg_attribute a
      LEFT JOIN pg_description ON a.attnum = pg_description.objsubid
      WHERE a.attrelid = ('"' || ? || '"')::regclass
      AND pg_table_is_visible(a.attrelid)
      AND a.attname = ?
    </template>
  </query>

  <query name="list-columns">
    <template engine="velocity">
      select
        T.COLUMN_NAME,
        T.DATA_TYPE,
        col_description(('"' || T.TABLE_NAME || '"')::regclass, T.ORDINAL_POSITION) COM,
        T.CHARACTER_MAXIMUM_LENGTH
      from INFORMATION_SCHEMA.COLUMNS T
      where T.TABLE_NAME = ?
      and T.TABLE_SCHEMA = CURRENT_SCHEMA()
      order by T.ORDINAL_POSITION
    </template>
  </query>

  <query name="drop-table">
    <!-- DROP TABLE [ IF EXISTS ] <table name> [ IF EXISTS ] <drop behavior> -->
    <template engine="velocity">
      DROP TABLE #if ($ifExists) IF EXISTS #end
      "$table"
      $dropBehaviour
    </template>
  </query>

  <query name="count-101">
    <template engine="velocity">
      SELECT COUNT(*) FROM (
      SELECT 1 FROM $tableName
          #if ($whereClause)
            WHERE $whereClause
          #end
      LIMIT 101
      ) SUBTABLE
    </template>
  </query>

  <query name="alter-column">
    <template engine="velocity">
      ALTER TABLE $table
      ALTER COLUMN $column TYPE $newtype
      #if ($using) USING $using #end
    </template>
  </query>
  <query name="add-column">
    <template engine="velocity">
        ALTER TABLE $table
        ADD COLUMN $columnDefinition
    </template>
  </query>

  <query name="read-data">
    <template engine="velocity">
      SELECT $fieldlist
      FROM $tablename TARGET_TABLE
      #if ($whereClause) WHERE $whereClause #end
      #if ($order) ORDER BY $order #end
      #if ($limit) LIMIT $limit #end
      #if ($offset) OFFSET $offset #end
    </template>
  </query>
  <query name="query-totals">
    <template engine="velocity">
      SELECT #foreach ($fieldDef in $fieldlist) #if ($velocityCount != 1) , #end
      $fieldDef
      #end
      FROM
      #if ($subquery) ( $subquery
        ) INTERNAL_RESULTS
      #else $tablename #end
      LIMIT 1
    </template>
  </query>
  <query name="get-last-generated-id">
    <template engine="velocity">
      select LASTVAL()
    </template>
  </query>

    <query name="monitoring-activity">
        <template engine="velocity">
            SELECT pid, waiting, state, query, query_start, backend_start, xact_start, state_change,
                   application_name, client_addr, client_hostname, client_port, datname, usename, usesysid,
                   CASE state WHEN 'active' THEN 1
                              WHEN 'idle in transaction' THEN 2
                              WHEN 'idle' THEN 3
                              ELSE 0
                   END orderby,
                   NOW() sysdate
            FROM pg_stat_activity
            WHERE query_start &lt; (current_timestamp - interval '1 second')
            #if ($pid) AND pid = $pid #end
            ORDER BY orderby ASC, query_start DESC
        </template>
    </query>
    <query name="monitoring-activity-kill">
        <template engine="velocity">SELECT pg_catalog.pg_terminate_backend($pid)</template>
    </query>
    <query name="monitoring-locks">
        <template engine="velocity">
        SELECT relation::REGCLASS, locktype, database, relation, page, tuple, virtualxid, transactionid,
                classid, objid, objsubid, virtualtransaction, pid, mode, granted, fastpath
        FROM pg_locks
        </template>
    </query>
    <query name="monitoring-cursors">
        <template engine="velocity">
        SELECT name, statement, is_holdable, is_binary, is_scrollable, creation_time
        FROM pg_cursors
        </template>
    </query>
    <query name="monitoring-blocking">
        <template engine="velocity">
        SELECT bl.pid                 AS blocked_pid,
            a.usename              AS blocked_user,
            ka.query               AS blocking_statement,
            now() - ka.query_start AS blocking_duration,
            kl.pid                 AS blocking_pid,
            ka.usename             AS blocking_user,
            a.query                AS blocked_statement,
            now() - a.query_start  AS blocked_duration
        FROM  pg_catalog.pg_locks         bl
        JOIN pg_catalog.pg_stat_activity a  ON a.pid = bl.pid
        JOIN pg_catalog.pg_locks         kl ON kl.transactionid = bl.transactionid AND kl.pid != bl.pid
        JOIN pg_catalog.pg_stat_activity ka ON ka.pid = kl.pid
        WHERE NOT bl.granted
        </template>
    </query>
</dialect>