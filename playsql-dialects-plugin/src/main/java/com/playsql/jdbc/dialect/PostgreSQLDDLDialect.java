package com.playsql.jdbc.dialect;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.playsql.dialect.api.DDLDialect;
import com.playsql.dialect.api.DialectInfo;
import com.playsql.dialect.model.DDLColumn;
import com.playsql.dialect.model.DDLTable;
import com.playsql.dialect.model.monitoring.Cursor;
import com.playsql.dialect.model.monitoring.Lock;
import com.playsql.dialect.model.monitoring.RunningQuery;
import com.playsql.jdbc.dialect.templates.TemplatesForPostgres;
import com.playsql.spi.CacheManager;
import com.playsql.spi.SpiUtils;
import com.playsql.spi.models.Tuple;
import com.playsql.spi.utils.Option;
import org.springframework.jdbc.core.RowMapper;

import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static com.playsql.dialect.api.DDLDialectMetadata.CaseSensibility.LOWER;
import static com.playsql.dialect.model.DDLColumn.Option.IDENTITY;
import static com.playsql.spi.utils.Option.*;
import static com.playsql.spi.utils.PlaySqlUtils.equal;
import static com.playsql.spi.utils.PlaySqlUtils.firstNonNull;

@DialectInfo(
        key = "com.playsql.jdbc.dialect.PostgreSQLDDLDialect",
        i18n = "PostgreSQL",
        defaultCase = LOWER,
        readWrite = true,
        autocomplete = true,
        monitoring = true,
        driverName = "org.postgresql.Driver",
        exampleUrl = "jdbc:postgresql://localhost:5432/db",
        minimumDriverString = "postgresql",
        minimumUrlString = "postgresql",
        titleLink = "http://www.play-sql.com",
        titleText = "The officially supported database"
)
public class PostgreSQLDDLDialect extends GenericDDLDialect implements DDLDialect
{
    private final static Set<String> RESERVED_KEYWORDS = Sets.newHashSet("A", "ABS", "ABSENT", "ACCORDING", "ADA", "ALL", "ALLOCATE",
            "ANALYSE", "ANALYZE", "AND", "ANY", "ARE", "ARRAY", "ARRAY_AGG", "ARRAY_MAX_CARDINALITY", "AS", "ASC",
            "ASENSITIVE", "ASYMMETRIC", "ATOMIC", "ATTRIBUTES", "AUTHORIZATION", "AVG", "BASE64", "BEGIN_FRAME",
            "BEGIN_PARTITION", "BERNOULLI", "BETWEEN", "BIGINT", "BINARY", "BIT", "BIT_LENGTH", "BLOB", "BLOCKED",
            "BOM", "BOOLEAN", "BOTH", "BREADTH", "C", "CALL", "CARDINALITY", "CASE", "CAST", "CATALOG_NAME", "CEIL",
            "CEILING", "CHAR", "CHARACTER", "CHARACTERS", "CHARACTER_LENGTH", "CHARACTER_SET_CATALOG",
            "CHARACTER_SET_NAME", "CHARACTER_SET_SCHEMA", "CHAR_LENGTH", "CHECK", "CLASS_ORIGIN", "CLOB", "COALESCE",
            "COBOL", "COLLATE", "COLLATION", "COLLATION_CATALOG", "COLLATION_NAME", "COLLATION_SCHEMA", "COLLECT",
            "COLUMN", "COLUMNS", "COLUMN_NAME", "COMMAND_FUNCTION", "COMMAND_FUNCTION_CODE", "CONCURRENTLY",
            "CONDITION", "CONDITION_NUMBER", "CONNECT", "CONNECTION_NAME", "CONSTRAINT", "CONSTRAINT_CATALOG",
            "CONSTRAINT_NAME", "CONSTRAINT_SCHEMA", "CONSTRUCTOR", "CONTAINS", "CONTROL", "CONVERT", "CORR",
            "CORRESPONDING", "COUNT", "COVAR_POP", "COVAR_SAMP", "CREATE", "CROSS", "CUBE", "CUME_DIST",
            "CURRENT_CATALOG", "CURRENT_DATE", "CURRENT_DEFAULT_TRANSFORM_GROUP", "CURRENT_PATH", "CURRENT_ROLE",
            "CURRENT_ROW", "CURRENT_SCHEMA", "CURRENT_TIME", "CURRENT_TIMESTAMP", "CURRENT_TRANSFORM_GROUP_FOR_TYPE",
            "CURRENT_USER", "CURSOR_NAME", "DATALINK", "DATE", "DATETIME_INTERVAL_CODE", "DATETIME_INTERVAL_PRECISION",
            "DB", "DEC", "DECIMAL", "DEFAULT", "DEFERRABLE", "DEFINED", "DEGREE", "DENSE_RANK", "DEPTH", "DEREF",
            "DERIVED", "DESC", "DESCRIBE", "DESCRIPTOR", "DETERMINISTIC", "DIAGNOSTICS", "DISCONNECT", "DISPATCH",
            "DISTINCT", "DLNEWCOPY", "DLPREVIOUSCOPY", "DLURLCOMPLETE", "DLURLCOMPLETEONLY", "DLURLCOMPLETEWRITE",
            "DLURLPATH", "DLURLPATHONLY", "DLURLPATHWRITE", "DLURLSCHEME", "DLURLSERVER", "DLVALUE", "DO", "DYNAMIC",
            "DYNAMIC_FUNCTION", "DYNAMIC_FUNCTION_CODE", "ELEMENT", "ELSE", "EMPTY", "END", "END-EXEC", "END_FRAME",
            "END_PARTITION", "ENFORCED", "EQUALS", "EVERY", "EXCEPT", "EXCEPTION", "EXEC", "EXISTS", "EXP", "EXPRESSION",
            "EXTRACT", "FALSE", "FETCH", "FILE", "FINAL", "FIRST_VALUE", "FLAG", "FLOAT", "FLOOR", "FOR", "FOREIGN",
            "FORTRAN", "FOUND", "FRAME_ROW", "FREE", "FREEZE", "FROM", "FS", "FULL", "FUSION", "G", "GENERAL",
            "GENERATED", "GET", "GO", "GOTO", "GRANT", "GREATEST", "GROUP", "GROUPING", "GROUPS", "HAVING", "HEX",
            "HIERARCHY", "ID", "IGNORE", "ILIKE", "IMMEDIATELY", "IMPLEMENTATION", "IMPORT", "IN", "INDENT",
            "INDICATOR", "INITIALLY", "INNER", "INOUT", "INSTANCE", "INSTANTIABLE", "INT", "INTEGER", "INTEGRITY",
            "INTERSECT", "INTERSECTION", "INTERVAL", "INTO", "IS", "ISNULL", "JOIN", "K", "KEY_MEMBER", "KEY_TYPE",
            "LAG", "LAST_VALUE", "LATERAL", "LEAD", "LEADING", "LEAST", "LEFT", "LENGTH", "LIBRARY", "LIKE",
            "LIKE_REGEX", "LIMIT", "LINK", "LN", "LOCALTIME", "LOCALTIMESTAMP", "LOCATOR", "LOWER", "M", "MAP",
            "MATCHED", "MAX", "MAX_CARDINALITY", "MEMBER", "MERGE", "MESSAGE_LENGTH", "MESSAGE_OCTET_LENGTH",
            "MESSAGE_TEXT", "METHOD", "MIN", "MOD", "MODIFIES", "MODULE", "MORE", "MULTISET", "MUMPS", "NAMESPACE",
            "NATIONAL", "NATURAL", "NCHAR", "NCLOB", "NESTING", "NEW", "NFC", "NFD", "NFKC", "NFKD", "NIL", "NONE",
            "NORMALIZE", "NORMALIZED", "NOT", "NOTNULL", "NTH_VALUE", "NTILE", "NULL", "NULLABLE", "NULLIF", "NUMBER",
            "NUMERIC", "OCCURRENCES_REGEX", "OCTETS", "OCTET_LENGTH", "OFFSET", "OLD", "ON", "ONLY", "OPEN", "OR",
            "ORDER", "ORDERING", "OTHERS", "OUT", "OUTER", "OUTPUT", "OVERLAPS", "OVERLAY", "OVERRIDING", "P", "PAD",
            "PARAMETER", "PARAMETER_MODE", "PARAMETER_NAME", "PARAMETER_ORDINAL_POSITION", "PARAMETER_SPECIFIC_CATALOG",
            "PARAMETER_SPECIFIC_NAME", "PARAMETER_SPECIFIC_SCHEMA", "PASCAL", "PASSTHROUGH", "PATH", "PERCENT",
            "PERCENTILE_CONT", "PERCENTILE_DISC", "PERCENT_RANK", "PERIOD", "PERMISSION", "PLACING", "PLI", "PORTION",
            "POSITION", "POSITION_REGEX", "POWER", "PRECEDES", "PRECISION", "PRIMARY", "PUBLIC", "RANK", "READS",
            "REAL", "RECOVERY", "REFERENCES", "REFERENCING", "REGR_AVGX", "REGR_AVGY", "REGR_COUNT", "REGR_INTERCEPT",
            "REGR_R2", "REGR_SLOPE", "REGR_SXX", "REGR_SXY", "REGR_SYY", "REQUIRING", "RESPECT", "RESTORE", "RESULT",
            "RETURN", "RETURNED_CARDINALITY", "RETURNED_LENGTH", "RETURNED_OCTET_LENGTH", "RETURNED_SQLSTATE",
            "RETURNING", "RIGHT", "ROLLUP", "ROUTINE", "ROUTINE_CATALOG", "ROUTINE_NAME", "ROUTINE_SCHEMA", "ROW",
            "ROW_COUNT", "ROW_NUMBER", "SCALE", "SCHEMA_NAME", "SCOPE", "SCOPE_CATALOG", "SCOPE_NAME", "SCOPE_SCHEMA",
            "SECTION", "SELECT", "SELECTIVE", "SELF", "SENSITIVE", "SERVER_NAME", "SESSION_USER", "SETOF", "SETS",
            "SIMILAR", "SIZE", "SMALLINT", "SOME", "SOURCE", "SPACE", "SPECIFIC", "SPECIFICTYPE", "SPECIFIC_NAME",
            "SQL", "SQLCODE", "SQLERROR", "SQLEXCEPTION", "SQLSTATE", "SQLWARNING", "SQRT", "STATE", "STATIC",
            "STDDEV_POP", "STDDEV_SAMP", "STRUCTURE", "STYLE", "SUBCLASS_ORIGIN", "SUBMULTISET", "SUBSTRING",
            "SUBSTRING_REGEX", "SUCCEEDS", "SUM", "SYMMETRIC", "SYSTEM_TIME", "SYSTEM_USER", "T", "TABLE",
            "TABLESAMPLE", "TABLE_NAME", "THEN", "TIES", "TIME", "TIMESTAMP", "TIMEZONE_HOUR", "TIMEZONE_MINUTE",
            "TO", "TOKEN", "TOP_LEVEL_COUNT", "TRAILING", "TRANSACTIONS_COMMITTED", "TRANSACTIONS_ROLLED_BACK",
            "TRANSACTION_ACTIVE", "TRANSFORM", "TRANSFORMS", "TRANSLATE", "TRANSLATE_REGEX", "TRANSLATION", "TREAT",
            "TRIGGER_CATALOG", "TRIGGER_NAME", "TRIGGER_SCHEMA", "TRIM", "TRIM_ARRAY", "TRUE", "UESCAPE", "UNDER",
            "UNION", "UNIQUE", "UNLINK", "UNNAMED", "UNNEST", "UNTYPED", "UPPER", "URI", "USAGE", "USER",
            "USER_DEFINED_TYPE_CATALOG", "USER_DEFINED_TYPE_CODE", "USER_DEFINED_TYPE_NAME", "USER_DEFINED_TYPE_SCHEMA",
            "USING", "VALUES", "VALUE_OF", "VARBINARY", "VARCHAR", "VARIADIC", "VAR_POP", "VAR_SAMP", "VERBOSE",
            "VERSIONING", "WHEN", "WHENEVER", "WHERE", "WIDTH_BUCKET", "WINDOW", "WITH", "XMLAGG", "XMLATTRIBUTES",
            "XMLBINARY", "XMLCAST", "XMLCOMMENT", "XMLCONCAT", "XMLDECLARATION", "XMLDOCUMENT", "XMLELEMENT",
            "XMLEXISTS", "XMLFOREST", "XMLITERATE", "XMLNAMESPACES", "XMLPARSE", "XMLPI", "XMLQUERY", "XMLROOT",
            "XMLSCHEMA", "XMLSERIALIZE", "XMLTABLE", "XMLTEXT", "XMLVALIDATE");

    @Inject
    public PostgreSQLDDLDialect(SpiUtils spiUtils, CacheManager cacheManager) {
        super(spiUtils, cacheManager);
    }

    /* Detail 101 of Postgres: entity names are considered lowercase unless quoted, in user's queries.
     * To be nice with them... If lowercase they be, lowercase they shall.
     */
    @Override
    public String escapeEntityName(String name) {
        return name.toLowerCase(Locale.ENGLISH).replaceAll("[^a-z0-9]", "_").replaceAll("^[_0-9]*", "");
    }

    @Override
    protected String toSQLColumnType(DDLColumn column) {
        String proposed = super.toSQLColumnType(column);
        if (column.has(IDENTITY)) {
            return "SERIAL PRIMARY KEY";
        }
        return proposed;
    }

    @Override
    protected Option parse(String dataType, Integer length) {
        if ("character varying".equals(dataType))
            return some(DDLColumn.DataType.VARCHAR);
        if ("NUMERIC".equalsIgnoreCase(dataType))
            return some(DDLColumn.DataType.NUMERIC);
        if ("FLOAT8".equals(dataType))
            return some(DDLColumn.DataType.DOUBLE);
        if ("integer".equals(dataType))
            return some(DDLColumn.DataType.INTEGER);
        if ("bigint".equals(dataType))
            return some(DDLColumn.DataType.BIG_INTEGER);
        if ("boolean".equalsIgnoreCase(dataType))
            return some(DDLColumn.DataType.BOOLEAN);
        if ("date".equalsIgnoreCase(dataType))
            return some(DDLColumn.DataType.DATE);
        if ("INTERVAL".equals(dataType))
            return some(DDLColumn.DataType.INTERVAL);
        if ("TIME".equals(dataType))
            return some(DDLColumn.DataType.TIME);
        if ("TIMESTAMP".equals(dataType))
            return some(DDLColumn.DataType.TIMESTAMP);
        if ("text".equals(dataType))
            return some(DDLColumn.DataType.TEXT);
        if ("bytea".equals(dataType))
            return some(DDLColumn.DataType.BYTEA);

        return none();
    }

    @Override
    public TemplatesForPostgres templates() {
        return new TemplatesForPostgres();
    }
/*
     *
     * ==============================================
     * ==          Direct commands                 ==
     * ==============================================
     */

    @Override
    public void duplicateTable(String origin, DDLTable table) {
        super.duplicateTable(origin, table);

        // Make the ID colum a SERIAL
        Integer maxId = firstNonNull(jdbc.queryForOne(templates().getMaxId(table.getName()), Integer.class), 1);
        String sequenceName = table.getName() + "_ID_seq";
        jdbc.execute(templates().createSequence(maxId, sequenceName, table.getName()));
    }

    @Override
    public void changeColumnType(DDLTable table, DDLColumn originalColumn, DDLColumn newColumn) {
        if (equal(newColumn.getName(), originalColumn.getName())) {
            // TODO hahaha, ahem, what about copying the data...
            // If data type mismatch, then add-copy-drop
            // If column rename, then add-copy-drop

            String newType = toSQLColumnType(newColumn);

            // Use conversion procedure?
            String using = null;
            switch (newColumn.getType()) {
                case DATE:
                    using = "CASE \"" + newColumn.getName() + "\" WHEN '' THEN NULL ELSE \"" + newColumn.getName() + "\"::" + newType + " END";
                    break;
                case VARCHAR:
                case TEXT:
                    break;
                case BIG_INTEGER:
                case INTEGER:
                case BOOLEAN:

                default:
                    if (originalColumn.getType() == DDLColumn.DataType.VARCHAR) {
                        using = "CASE \"" + newColumn.getName() + "\" WHEN '' THEN NULL ELSE \"" + newColumn.getName() + "\"::" + newType + " END";
                    } else {
                        using = "\"" + newColumn.getName() + "\"::" + newType;
                    }
                    break;
            }

            jdbc.execute(templates().alterColumn(
                quoteEntityName(newColumn.getName()),
                newType, quoteEntityName(table.getName()), using));
        }
    }

    @Override
    public void upgradeTask2_upgradeExistingTables() {
        List<Tuple<String, String>> names = listPlaySqlTableNames(true);

        DDLColumn positionColumn = DDLColumn.getPositionColumnDDL();
        String newType = toSQLColumnType(positionColumn);

        for (Tuple<String, String> table : names) {
            jdbc.execute(
                templates().alterColumn(
                    quoteEntityName(positionColumn.getName()),
                    newType,
                    quoteEntityName(table.first()),
                    null));
        }
    }

    @Override
    public List<RunningQuery> getMonitoringActivity(Long pid) {
        return jdbc.query(
            templates().monitoringActivity(pid), (rs, rowNum) -> new RunningQuery(
                                firstNonNull(rs.getString("pid"), ""),
                                firstNonNull(rs.getString("state"), ""),
                                firstNonNull(rs.getString("query"), ""),
                                rs.getTimestamp("xact_start"))
                                .setApplication_name(firstNonNull(rs.getString("application_name"), ""))
                                .setClient_addr(firstNonNull(rs.getString("client_addr"), ""))
                                .setClient_hostname(firstNonNull(rs.getString("client_hostname"), ""))
                                .setClient_port(firstNonNull(rs.getString("client_port"), ""))
                                .setDatname(firstNonNull(rs.getString("datname"), ""))
                                .setUsename(firstNonNull(rs.getString("usename"), ""))
                                .setUsesysid(firstNonNull(rs.getString("usesysid"), ""))
                                .setBackend_start(rs.getTimestamp("backend_start"))
                                .setQuery_start(rs.getTimestamp("query_start"))
                                .setState_change(rs.getTimestamp("state_change"))
                                .setSysdate(rs.getTimestamp("sysdate"))
        );
    }

    public String monitoringTerminateBackend(Long pid) {
        return jdbc.queryForOne(
            templates().monitoringActivityKill(pid),
            String.class);
    }

    @Override
    public List<Lock> getMonitoringLocks() {
        return jdbc.query(templates().monitoringLocks(), (rs, rowNum) -> new Lock(
                firstNonNull(rs.getString("relation"), ""),
                firstNonNull(rs.getString("mode"), ""),
                firstNonNull(rs.getString("locktype"), ""),
                firstNonNull(rs.getString("page"), ""),
                firstNonNull(rs.getString("tuple"), ""),
                firstNonNull(rs.getString("transactionid"), ""),
                firstNonNull(rs.getString("pid"), ""),
                firstNonNull(rs.getString("granted"), ""),
                firstNonNull(rs.getString("fastpath"), "")
        ));
    }

    @Override
    public List<Cursor> getMonitoringCursors() {
        return jdbc.query(templates().monitoringCursors(),
                new RowMapper() {
                    @Override
                    public Cursor mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new Cursor(
                                firstNonNull(rs.getString("name"), ""),
                                firstNonNull(rs.getString("statement"), ""),
                                firstNonNull(rs.getString("is_holdable"), ""),
                                firstNonNull(rs.getString("is_binary"), ""),
                                firstNonNull(rs.getString("is_scrollable"), ""),
                                firstNonNull(rs.getString("creation_time"), "")
                        );
                    }
                });
    }

    @Override
    public List<Map<String, String>> getMonitoringBlockingActivity() {
        return jdbc.query(templates().monitoringBlocking(),
                new RowMapper() {
                    @Override
                    public Map<String, String> mapRow(ResultSet rs, int rowNum) throws SQLException {
                        Map<String, String> map = Maps.newHashMap();
                        int columnCount = rs.getMetaData().getColumnCount();
                        for (int i = 1 ; i <= columnCount; i++) {
                            map.put(rs.getMetaData().getColumnName(i),
                                    rs.getString(i));
                        }
                        return map;
                    }
                });
    }

    @Override
    public boolean isReservedKeyword(String w) {
        if (super.isReservedKeyword(w)) return true;
        return RESERVED_KEYWORDS.contains(w.toUpperCase(Locale.ENGLISH));
    }
}