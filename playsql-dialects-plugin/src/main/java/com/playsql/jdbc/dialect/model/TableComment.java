package com.playsql.jdbc.dialect.model;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.dialect.model.TotalColumn;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * This class helps jsonize the metadata about a table, to store it in
 * the table comments.
 *
 * <p/>
 * Since dialects are free to store metadata in the table comments or in a
 * system table, this class must stay together with <i>implementations</i> of
 * dialects.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TableComment {
    private String label;

    /** The order of columns. If a column is unknown, this comment may be updated */
    @XmlElement(name = "columnOrder")
    private List<String> columnOrder;
    @XmlElement(name = "totals")
    private List<TotalColumn> totals;

    public TableComment(String label, List<TotalColumn> totals, List<String> columnOrder) {
        this.label = label;
        this.totals = totals;
        this.columnOrder = columnOrder;
    }

    public TableComment() {
    }

    public String label() {
        return label;
    }

    public void label(String label) {
        this.label = label;
    }

    public List<TotalColumn> totals() {
        return totals;
    }

    public void totals(List<TotalColumn> totals) {
        this.totals = totals;
    }

    public List<String> columnOrder() {
        return columnOrder;
    }

    public void setColumnOrder(List<String> columnOrder) {
        this.columnOrder = columnOrder;
    }

}
