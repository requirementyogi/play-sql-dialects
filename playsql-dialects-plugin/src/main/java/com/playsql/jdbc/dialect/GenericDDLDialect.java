package com.playsql.jdbc.dialect;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.gson.JsonSyntaxException;
import com.playsql.dialect.api.*;
import com.playsql.dialect.exceptions.CircularFormulaException;
import com.playsql.dialect.exceptions.JdbcException;
import com.playsql.dialect.model.*;
import com.playsql.dialect.model.ForeignKey.Action;
import com.playsql.dialect.model.audittrail.AuditTrailItem;
import com.playsql.dialect.model.audittrail.OperationType;
import com.playsql.dialect.model.formulas.Endpoint;
import com.playsql.dialect.model.formulas.FormulaDependency;
import com.playsql.dialect.model.monitoring.Cursor;
import com.playsql.dialect.model.monitoring.Lock;
import com.playsql.dialect.model.monitoring.RunningQuery;
import com.playsql.jdbc.dialect.model.ColumnComment;
import com.playsql.jdbc.dialect.model.SchemaComment;
import com.playsql.jdbc.dialect.model.TableComment;
import com.playsql.jdbc.dialect.templates.TemplatesForGeneric;
import com.playsql.spi.CacheManager;
import com.playsql.spi.SpiUtils;
import com.playsql.spi.models.Tuple;
import com.playsql.spi.models.Tuple.Truple;
import com.playsql.spi.utils.Option;
import com.playsql.spi.utils.PlaySqlUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.filter;
import static com.playsql.dialect.api.DDLDialectMetadata.CaseSensibility.UPPER;
import static com.playsql.dialect.model.DDLColumn.Option.*;
import static com.playsql.spi.utils.Option.*;
import static com.playsql.spi.utils.PlaySqlUtils.equal;
import static com.playsql.spi.utils.PlaySqlUtils.firstNonNull;
import static java.util.Locale.ENGLISH;

@DialectInfo(
        key = "com.playsql.jdbc.dialect.GenericDDLDialect",
        i18n = "Generic",
        defaultCase = UPPER,
        driverName = "com.example.Driver",
        exampleUrl = "jdbc:drivername://localhost:1111/dbname"
)
public class GenericDDLDialect extends AbstractDDLDialect {

    final static Logger log = LoggerFactory.getLogger(GenericDDLDialect.class.getCanonicalName());

    /** We keep a reference to the driver locally because {@link DriverManager} isn't
     * always able to do its job.
     class GenericDDLDialect extends AbstractDDLDialect {

    final static Logger log = LoggerFactory.getLogger(GenericDDLDialect.class.getCanonicalName());

    /** We keep a reference to the driver locally because {@link DriverManager} isn't
     * always able to do its job.
     */
    final private static Map<String, Driver> drivers = new ConcurrentHashMap<>();

    final protected SpiUtils spiUtils;
    final protected CacheManager.Cache<DDLTable> tableDefinitionCache;

    @Inject
    public GenericDDLDialect(SpiUtils spiUtils, CacheManager cacheManager) {
        super();
        this.spiUtils = spiUtils;
        this.tableDefinitionCache = cacheManager.getCache("table-definitions", DDLTable.class, true, new Function<DDLTable, DDLTable>(){
            @Override
            public DDLTable apply(DDLTable input) {
                if (input == null) return null;
                return input.copy(null);
            }
        });
    }

    /**
     * The client (CloudSpiUtils or Confluence) is in charge of calling this method in case of
     * AbstractTableEvent
     *
     * Note: Probable bug: If there's the same table in several contexts, there's probably a cache collision!
     */
    public void purgeCache(String tableName) {
        if (jdbc != null) {
            tableDefinitionCache.remove(getTableDefinitionCacheKey(tableName));
        } else {
            tableDefinitionCache.removeAll();
        }
    }

    @Override
    public String escapeEntityName(String name) {
        return name.toUpperCase(ENGLISH).replaceAll("[^A-Z0-9]", "_").replaceAll("^[_0-9]*", "");
    }

    public boolean isReservedKeyword(String w) {
        return false;
    }

    /** Puts the name of a column or table between quotes */
    @Override
    public String quoteEntityName(String name) {
        /*
        AVST couldn't INSERT INTO a table because their table name was lowercase, but strange.
        Besides, we can always escape, it doesn't hurt.
        if (org.apache.commons.lang3.StringUtils.isAllLowerCase(name))
            if (!isReservedKeyword(name))
                return name;*/
        return backtick() + name.replaceAll("([\\\"])", "\\\\$1") + backtick();
    }

    /** Only to be used by {@link #quoteEntityName}. Implementations can override it. */
    protected String backtick() {
        return "\"";
    }

    @Override
    public String makeSchemaName(String userEntry) {
        // We use lowercase, because table names in PostGres are considered lowercase by default.
        String name = escapeEntityName(userEntry);
        if (name.length() == 0) {
            throw new IllegalArgumentException("Name is not suitable in this dialect. Must start with a letter and contain alphanumeric characters: " + name);
        }
        return name;
    }

    public Driver getDriverInstance(String driver) {
        // "If you need a "real" connection pool outside of a J2EE container, consider Apache's Jakarta Commons DBCP or C3P0."
        Driver result = drivers.get(driver);
        if (result != null) {
            return result;
        }
        result = getDriverManager(driver);
        if (result != null) {
            log.debug("Just found " + driver + " using DriverManager.getDrivers()");
            drivers.put(driver, result);
            return result;
        }

        // We actually need to load the driver
        try {
            Class<?> driverClass;
            try {
                // If the driver is in the plugin of the current dialect.
                // If the class doesn't exist in the plugin, it will lookup Confluence's classloader.
                // Please keep this option as first because we want to load the plugin's driver in
                // case it's an override of the parent classloader's driver.
                driverClass = Class.forName(driver);
                log.debug("Just loaded " + driver + " using forClass()");
            } catch (ClassNotFoundException cnfe) {
                /* Note GenericDDLDialect in the following command: If the current dialect is
                 * an _implementation_ of the GenericDDLDialect, but located in another plugin,
                 * then the command {@code Class.forName(driver)} above didn't have a chance
                 * to check in the classloader of GneericDDLDialect, and that's what we
                 * do here.
                 */
                driverClass = spiUtils.loadClass(driver, GenericDDLDialect.class);
                if (driverClass == null) {
                    throw new RuntimeException("Can't find the driver: " + driverClass);
                }
                log.debug("Just loaded " + driver + " after a CNFE: " + cnfe.getMessage());
            }
            result = getDriverManager(driver);
            // Is it there yet?
            if (result == null) {
                result = (Driver) driverClass.newInstance();
            }
            drivers.put(driver, result);
            return result;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Can't find driver: " + driver, e);
        } catch (SecurityException e) {
            throw new RuntimeException("Can't use driver: " + driver, e);
        } catch (InstantiationException e) {
            throw new RuntimeException("Error while instantiating the JDBC driver", e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Error while instantiating the JDBC driver", e);
        }
    }

    /** Note that you may want to override this method <i>with the exact same implementation <b>yes!</b></i>,
     * in the situation where your dialect is in a separate plugin. That's because there may be two
     * DriverManagers in two different classloaders.
      * @param driver
     * @return
     */
    protected static Driver getDriverManager(String driver) {
        Driver instance;
        Enumeration<Driver> list = DriverManager.getDrivers();
        while (list.hasMoreElements()) {
            instance = list.nextElement();
            // We can't use instanceof because it may come from another classloader.
            if (instance != null && instance.getClass().getCanonicalName().equals(driver)) {
                return instance;
            }
        }
        return null;
    }

    @Override
    public CurrentSessionInformation getCurrentUserAndSchema() {

        SQLStatement stmt;
        stmt = new SQLStatement(templates().currentUserAndSchema());

        List<CurrentSessionInformation> results = jdbc.query(stmt, new RowMapper() {
            @Override
            public CurrentSessionInformation mapRow(ResultSet rs, int rowNum) throws SQLException {
                return new CurrentSessionInformation(rs.getString(1), rs.getString(2));
            }
        });

        return results.get(0);
    }

    @Override
    public String sqlSelectTable(String table) {
        return templates().selectTable(quoteEntityName(table));
    }

    protected String toSQLColumnType(DropBehaviour behaviour) {
        switch (behaviour) {
            case CASCADE:
                return "CASCADE";
            case RESTRICT:
                return "RESTRICT";
        }
        throw new NotImplementedException();
    }

    // <editor-fold desc="Gson-related functions">
    protected TableComment gsonTable(String json) {
        try {
            if (json != null) {
                TableComment tableComment = PlaySqlUtils.gson().fromJson(json, TableComment.class);
                if (tableComment != null) return tableComment;
            }
            return new TableComment();
        } catch (JsonSyntaxException jse) {
            return new TableComment();
        }
    }

    protected ColumnComment gsonColumn(String json) {
        if (json == null) return new ColumnComment();
        try {
            return PlaySqlUtils.gson().fromJson(json, ColumnComment.class);
        } catch (JsonSyntaxException jse) {
            return new ColumnComment();
        }
    }

    protected SchemaComment gsonSchema(String json) {
        if (json == null) return new SchemaComment();
        try {
            return PlaySqlUtils.gson().fromJson(json, SchemaComment.class);
        } catch (JsonSyntaxException jse) {
            return new SchemaComment();
        }
    }

    protected String gson(ColumnComment columnComment) {
        return PlaySqlUtils.gson().toJson(columnComment);
    }

    protected String gson(TableComment tableComment) {
        return PlaySqlUtils.gson().toJson(tableComment);
    }

    protected String gson(SchemaComment schemaComment) {
        return PlaySqlUtils.gson().toJson(schemaComment);
    }

    @Override
    public TemplatesForGeneric templates() {
        return new TemplatesForGeneric();
    }


    // </editor-fold>

    /*
     *
     * ==============================================
     * ==          Direct commands                 ==
     * ==============================================
     */



    //</editor-fold>

    @Override
    public boolean hasTable(String table) {
        SQLStatement sql = new SQLStatement(templates().hasTable()).addArgument(table);
        Integer count = jdbc.queryForOne(sql, Integer.class);
        switch (count) {
            case 0: return false;
            case 1: return true;
            default: throw new RuntimeException("Unexpected answer '" + count + "' for " + sql);
        }
    }

    @Override
    public void createTable(DDLTable table) {
        List<String> columns = Lists.newArrayList();
        for (DDLColumn column : table.getColumns()) {
            columns.add(concatOptional(columnDefinition(column), fkConstraint(column)));
        }

        jdbc.execute(templates().createTable(columns, table.getName()));

        // Set the column labels
        setColumnCommentsForAllColumns(table);

        tableDefinitionCache.remove(getTableDefinitionCacheKey(table.getName()));
    }

    @Override
    public void duplicateTable(String origin, DDLTable table) {
        // Copy the contents
        jdbc.execute(templates().copyTable(origin, table.getName()));

        // Put the column comments
        setColumnCommentsForAllColumns(table);

        tableDefinitionCache.remove(getTableDefinitionCacheKey(table.getName()));
    }

    protected void setColumnCommentsForAllColumns(DDLTable table) {
        setCommentOnTable(table.getName(), new TableComment(table.getLabel(), table.getTotals(), table.getColumnOrder()));
        for (DDLColumn column : table.getColumns()) {
            setColumnComment(table, column);
        }
        tableDefinitionCache.remove(getTableDefinitionCacheKey(table.getName()));
    }

    private void setColumnComment(DDLTable table, DDLColumn column) {
        ColumnComment comment = new ColumnComment(
            column.getLabel(),
            column.getRenderer(),
            column.getViewRenderer(),
            column.getFormula(),
            column.getWidth(),
            column.getFormats()
        );
        setFkOnColumnComment(column, column.getFk(), comment);
        setColumnComment(table.getName(), column.getName(), comment);
        tableDefinitionCache.remove(getTableDefinitionCacheKey(table.getName()));
    }

    private void setFkOnColumnComment(DDLColumn column, ForeignKey fk, ColumnComment comment) {
        if (fk != null) {
            comment.fkReset();
            if (fk.isWithSQLConstraint() || fk.isWithUI()) {
                if (!fk.isWithSQLConstraint()) {
                    comment.setFkWithoutSQLConstraint(fk.getTargetTable(), fk.getTargetPK());
                }
                if (!fk.isWithUI()) {
                    comment.setFkWithoutUI();
                }
                comment.fkDisplayField(fk.getDisplayField());
                comment.fkSearchField(fk.getSearchField());
            } else {
                // There should have been no value in column.fk to begin with.
                column.setFk(null);
            }
        } else {
            comment.fkReset();
        }
    }

    protected void setColumnComment(String tablename, String columnname, ColumnComment comment) {
        String gson = gson(comment);
        jdbc.update(
            templates().setCommentOnColumn(
                quoteEntityName(tablename),
                quoteEntityName(columnname),
                quoteString(gson)
                ));
        tableDefinitionCache.remove(getTableDefinitionCacheKey(tablename));
    }

    protected ColumnComment getCommentOnColumn(String tablename, String columnname) {
        String json = jdbc.queryForOne(
            templates().getCommentOnColumn(),
            new Object[]{tablename, columnname}, String.class);
        return gsonColumn(json);
    }

    @Override
    public void dropTable(DDLTable table, boolean ifExists, DropBehaviour behaviour) {
        jdbc.execute(templates().dropTable(toSQLColumnType(behaviour), ifExists, table.getName()));

        tableDefinitionCache.remove(getTableDefinitionCacheKey(table.getName()));
    }

    @Override
    public void updateTableMetadata(DDLTable table) {
        setCommentOnTable(table.getName(), new TableComment(table.getLabel(), table.getTotals(), table.getColumnOrder()));
    }

    @Override
    public void addColumn(DDLTable table, DDLColumn column, DDLColumn beforeColumn) {
        jdbc.execute(templates().addColumn(
            beforeColumn != null ? "BEFORE " + beforeColumn.getName() : null,
            concatOptional(columnDefinition(column), fkConstraint(column)),
            quoteEntityName(table.getName())));
        setColumnComment(table, column);

        tableDefinitionCache.remove(getTableDefinitionCacheKey(table.getName()));
    }

    @Override
    public void dropColumn(DDLTable table, DDLColumn column, DropBehaviour behaviour) {
        jdbc.execute(templates().dropColumn(toSQLColumnType(behaviour), column.getName(), table.getName()));

        tableDefinitionCache.remove(getTableDefinitionCacheKey(table.getName()));
    }

    /**
     * Reads a binary cell. It passes the InputStream to the reader and closes it after, whether or not
     * the reader threw an exception.
     */
    public void getBinaryData(String tableName, String columnName, int id, Consumer<InputStream> reader) {
        SQLStatement sqlStatement = new SQLStatement(templates().getBinaryData(tableName, columnName, id));
        jdbc.queryForOne(sqlStatement, (rs, rowNum) -> {
            InputStream binaryStream = rs.getBinaryStream(1);
            try {
                if (rs.wasNull()) {
                    reader.accept(null);
                } else {
                    reader.accept(binaryStream);
                }
            } finally {
                if (binaryStream != null) {
                    try {
                        binaryStream.close();
                    } catch (IOException e) {
                        // Do nothing
                    }
                }
            }
            return null;
        });
    }

    @Override
    public void copyBinaryData(
        String fromTable, String fromColumn, int fromId,
        String toTable, String toColumn, int toId) {
        jdbc.execute(templates().copyBinaryData(
            quoteEntityName(fromTable), quoteEntityName(fromColumn), fromId,
            quoteEntityName(toTable), quoteEntityName(toColumn), toId));
    }

    @Override
    public  void updateBinaryData(String tableName, String columnName, int id, byte[] data){
        String template = templates().writeData(Lists.newArrayList(quoteEntityName(columnName)), id, quoteEntityName(tableName));
        jdbc.update(template, new Object[] { data });
    }

    @Override
    public void renameColumn(DDLTable table, DDLColumn originalColumn, DDLColumn newColumn) {
        jdbc.execute(templates().renameColumn(newColumn.getName(), originalColumn.getName(), table.getName()));

        tableDefinitionCache.remove(getTableDefinitionCacheKey(table.getName()));
    }

    @Override
    public void renameTable(String original, String newName) {
        jdbc.execute(templates().renameTable(newName, original));

        tableDefinitionCache.remove(getTableDefinitionCacheKey(original));
    }

    @Override
    public void changeColumnType(DDLTable table, DDLColumn originalColumn, DDLColumn newColumn) {
        if (equal(newColumn.getName(), originalColumn.getName())) {
            // TODO hahaha, ahem, what about copying the data...
            // If data type mismatch, then add-copy-drop
            // If column rename, then add-copy-drop
            jdbc.execute(templates().alterColumn(columnDefinition(newColumn), quoteEntityName(table.getName())));
        } else {
            throw new RuntimeException("The method to change the type of a column was called, but its name has changed too, from " + originalColumn.getName() + " to " + newColumn.getName() + ".");
        }
        tableDefinitionCache.remove(getTableDefinitionCacheKey(table.getName()));
    }

    @Override
    public void moveColumn(DDLTable definition, String columnname, String before) {
        throw new NotImplementedException("Can't move columns in Hsqldb");
    }

    @Override
    public void changeColumnLabelFormulaWidthFKAndRenderer(DDLTable definition, DDLColumn originalColumn, DDLColumn column) {
        ColumnComment comment = getCommentOnColumn(definition.getName(), originalColumn.getName());
        comment.label(column.getLabel());
        comment.formula(column.getFormula());
        comment.renderer(column.getRenderer());
        comment.viewRenderer(column.getViewRenderer());
        comment.width(column.getWidth());
        setFkOnColumnComment(column, column.getFk(), comment);

        setColumnComment(definition.getName(), originalColumn.getName(), comment);
    }

    @Override
    public void changeColumnFormats(DDLTable definition, DDLColumn originalColumn, DDLColumn column) {
        ColumnComment comment = getCommentOnColumn(definition.getName(), originalColumn.getName());
        comment.formats(column.getFormats());
        setColumnComment(definition.getName(), originalColumn.getName(), comment);
    }

    /**
     * Only returns tables which have parseable labels. The reason is, we want to know Play SQL tables.
     * @return
     */
    public List<Tuple<String, String>> listPlaySqlTableNames(final boolean includeDeleted) {
        Object list = filter(jdbc.query(new SQLStatement(templates().listTablesAndComments()), (rs, rowNum) -> {
            String tableName = rs.getString("tablename");
            if (tableName.startsWith(AuditTrailItem.DELETED_PREFIX) && !includeDeleted) {
                return null;
            }
            String tableComment = rs.getString("com");
            TableComment comment = gsonTable(tableComment);
            if (StringUtils.isBlank(comment.label())) {
                return null;
            }
            return new Tuple<String, String>(tableName, comment.label());
        }), Predicates.notNull());
        return Lists.newArrayList((Iterable<Tuple<String, String>>) list);
    }

    public Multimap<String, String> listTables(final String schema, final boolean allSchemas, final String startingWith, final boolean withIntegerColumn) {
        String stmt;
        String[] args;
        if (allSchemas) {
            stmt = templates().listTablesWithoutSchema(StringUtils.isNotBlank(startingWith), withIntegerColumn, false);
            if (startingWith != null)
                args = new String[] {startingWith + "%"};
            else
                args = new String[] {};
        } else if (StringUtils.isBlank(schema)) {
            stmt = templates().listTablesWithoutSchema(StringUtils.isNotBlank(startingWith), withIntegerColumn, true);
            if (startingWith != null)
                args = new String[] {startingWith + "%"};
            else
                args = new String[] {};
        } else {
            stmt = templates().listTablesWithSchema(StringUtils.isNotBlank(startingWith), withIntegerColumn);
            if (startingWith != null)
                args = new String[] {schema, startingWith + "%"};
            else
                args = new String[] {schema};
        }
        // Group them per schema
        Multimap<String, String> tables = HashMultimap.create();
        for (String[] entry : jdbc.query(stmt, args, String[].class)) {
            tables.put(entry[0], entry[1]);
        }
        return tables;
    }

    @Override
    public int countUpTo101(String tableName) {

        return jdbc.queryForOne(new SQLStatement(
            templates().count101(quoteEntityName(tableName), null)
        ), Integer.class);
    }

    @Override
    public <T> List<T> readData(String tablename, String selectFields,
                                String whereClause,
                                String order,
                                Long limit, Long offset,
                                String pkvalue, ResultSetExtractor resultSetExtractor) {
        if (pkvalue != null) whereClause = backtick() + "ID" + backtick() + " = " + Integer.parseInt(pkvalue);
        SQLStatement sql = new SQLStatement(templates().readData(selectFields, limit, offset, order, tablename, whereClause));
        return jdbc.query(sql, resultSetExtractor);
    }

    /**
     * Read 1 row
     * @param tablename the table name. Not escaped.
     * @param field the field to read. Not escaped.
     * @param pkvalue the value of the ID column
     * @param resultType the class of the result type
     * @return the value, or null
     */
    public <T> T readRecord(String tablename, String field, String pkColumn, int pkvalue, Class<T> resultType) {
        String whereClause = quoteEntityName(pkColumn) + " = " + pkvalue;
        SQLStatement sql = new SQLStatement(templates().readData(quoteEntityName(field), 1L, null, null, quoteEntityName(tablename), whereClause));
        return jdbc.queryForOne(sql, resultType);
    }

    @Override
    public ReadDataFunction getStatementForReadData() {
        return templates()::readData;
    }

    @Override
    public void createAuditTrailTable() {
        String sql = templates().createAuditTrailTable();
        jdbc.execute(sql);
    }

    public void saveAuditTrailItem(AuditTrailItem item) {
        SQLStatement sql = new SQLStatement(templates().insertAuditTrail(),
                item.getOperation().toString(),
                item.getTableName(),
                item.getColumnName(),
                item.getRowId(),
                item.getAuthor(),
                item.getTimestamp(),
                item.getComments(),
                Objects.toString(item.getDataType(), null),
                item.getOldValue(),
                item.getNewValue());
        jdbc.insert2(sql);
        //item.setId(result);
    }

    @Override
    public List<AuditTrailItem> listAuditTrail(String tableName, String columnName, Integer rowId,
                                               String author, int limit, int offset) {
        List<Object> args = Lists.newArrayList();
        boolean hasTableName = tableName != null;
        boolean hasColumnName = columnName != null;
        boolean hasRowID = rowId != null;
        boolean hasAuthor = author != null;
        if (hasTableName) args.add(tableName);
        if (hasColumnName) args.add(columnName);
        if (hasRowID) args.add(rowId);
        if (hasAuthor) args.add(author);

        SQLStatement sql = new SQLStatement(templates().readAuditTrail(hasAuthor, hasColumnName, hasRowID, hasTableName, true, limit, offset), args);

        return getAuditTrailItems(sql);
    }

    private List<AuditTrailItem> getAuditTrailItems(SQLStatement sql) {
        List<AuditTrailItem> result = jdbc.query(sql, new ResultSetExtractor() {
            @Override
            public List<AuditTrailItem> extractData(ResultSet rs) throws SQLException {
                List<AuditTrailItem> list = Lists.newArrayList();
                while (rs.next()) {
                    AuditTrailItem item;
                    item = new AuditTrailItem(
                        OperationType.valueOf(rs.getString("operation")),
                        rs.getString("table_name"),
                        orNull(rs.getString("column_name"), rs.wasNull()),
                        orNull(rs.getInt("row_id"), rs.wasNull()),
                        rs.getString("author"),
                        toCalendar(rs.getTimestamp("timestamp")),
                        orNull(rs.getString("comments"), rs.wasNull()),
                        dataTypeOf(rs.getString("data_type")),
                        orNull(rs.getString("old_val"), rs.wasNull()),
                        orNull(rs.getString("new_val"), rs.wasNull())
                    );
                    list.add(item);
                }
                return list;
            }

            private DDLColumn.DataType dataTypeOf(String dataType) {
                if (dataType == null) return null;
                return DDLColumn.DataType.valueOf(dataType);
            }

            private Calendar toCalendar(Timestamp date) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                return cal;
            }
        });
        return result;
    }

    @Override
    public List<AuditTrailItem> auditTrailTrimByDays(Integer duration) {
        List<AuditTrailItem> itemsRequiringExternalDeletion = getAuditTrailItems(
            new SQLStatement(templates().auditTrailTrimByDaysPrerequisites(), duration));
        jdbc.insert2(new SQLStatement(templates().auditTrailTrimByDays(), duration));
        return itemsRequiringExternalDeletion;
    }

    @Override
    public List<AuditTrailItem> auditTrailTrimByCount(Integer count) {
        List<String> tablesToTrim = jdbc.query(new SQLStatement(templates().auditTrailTrimByCount1(), count), String.class);
        List<AuditTrailItem> itemsRequiringExternalDeletion = Lists.newArrayList();
        for (String table : tablesToTrim) {
            itemsRequiringExternalDeletion.addAll(getAuditTrailItems(
                    new SQLStatement(templates().auditTrailTrimByCountPrerequisites(), table, count)));
            jdbc.insert2(new SQLStatement(templates().auditTrailTrimByCount2(), table, count));
        }
        return itemsRequiringExternalDeletion;
    }

    @Override
    public List<RunningQuery> getMonitoringActivity(Long id) {
        throw new NotImplementedException("getMonitoringActivity for " + this.getClass().getCanonicalName());
    }

    @Override
    public List<Lock> getMonitoringLocks() {
        throw new NotImplementedException("getMonitoringLocks for " + this.getClass().getCanonicalName());
    }

    @Override
    public List<Cursor> getMonitoringCursors() {
        throw new NotImplementedException("getMonitoringCursors for " + this.getClass().getCanonicalName());
    }

    @Override
    public List<Map<String, String>> getMonitoringBlockingActivity() {
        throw new NotImplementedException("getMonitoringBlockingActivity for " + this.getClass().getCanonicalName());
    }

    @Override
    public String monitoringTerminateBackend(Long id) {
        throw new NotImplementedException("monitoringTerminateBackend for " + this.getClass().getCanonicalName());
    }

    public <T> T orNull(T value, boolean isNull) {
        if (isNull) return null;
        return value;
    }

    @Override
    public void createSettingsTable() {
        String sql = templates().createSettingsTable();
        jdbc.execute(sql);
    }

    @Override
    public String getSettings(String category, String key) {
        SQLStatement sql;
        if (category == null) {
            sql = new SQLStatement(templates().getSettingsWithoutCategory(), key);
        } else {
            sql = new SQLStatement(templates().getSettingsWithCategory(), key, category);
        }
        return jdbc.queryForOne(sql, String.class);
    }

    @Override
    public void setSettings(String category, String key, String value) {
        if (category != null) {
            throw new NotImplementedException("Not implemented: Settings with a category");
        }
        jdbc.update(new SQLStatement(templates().deleteSettings(), key));
        if (value != null) {
            jdbc.update(new SQLStatement(templates().insertSettings(), null, key, value));
        }
    }

    @Override
    public Integer writeData(String tablename, List<String> colNamesList,
                          List<Object> arguments, String pkvalue) {
        Long pkValueInt = pkvalue != null ? Long.parseLong(pkvalue) : null;

        SQLStatement sql = new SQLStatement(
            templates().writeData(colNamesList, pkValueInt, quoteEntityName(tablename)),
            arguments);
        return jdbc.update(sql);
    }

    @Override
    public String insertData(String tablename, List<String> colNamesList,
                             List<Object> arguments) {
        SQLStatement sql = new SQLStatement(
            templates().insertData(colNamesList, quoteEntityName(tablename)));
        sql.setArguments(arguments);
        jdbc.insert2(sql);
        SQLStatement getID = new SQLStatement(templates().getLastGeneratedId());
        String id = jdbc.queryForOne(getID.sql(), String.class);
        return id;
    }

    @Override
    public int removeData(String tablename, String pkvalue) {
        Integer pkValueInt = pkvalue != null ? Integer.parseInt(pkvalue) : null;

        SQLStatement sql = new SQLStatement(templates().removeData(pkValueInt, tablename));
        return jdbc.update(sql);
    }

    @Override
    public <T> T queryForTotals(List<String> fieldList,
                                String subquery,
                                List<Object> arguments,
                                ResultSetExtractor applier) {

        SQLStatement sql = new SQLStatement(
            templates().queryTotals(fieldList, subquery, null),
                arguments
        );
        try {
            List<?> result = jdbc.query(sql, applier);
            return (T) result;
        } catch (JdbcException re) {
            GenericDDLDialect.log.debug("Exception while running a query for totals", re);
            return null; // The message is sent another way
        }
    }

    @Override
    public boolean hasSchema(String schema) {
        SQLStatement sql = new SQLStatement(templates().hasSchema())
                .addArgument(schema);
        return 1 == jdbc.queryForOne(sql, Integer.class);
    }

    @Override
    public void createSchema(String schema) {
        SQLStatement sql = new SQLStatement(templates().createSchema(schema));
        jdbc.update(sql);
    }

    @Override
    public void dropSchema(String schema) {
        SQLStatement sql = new SQLStatement(templates().dropSchema(schema));
        jdbc.update(sql);
    }

    @Override
    public void setDefaultSchema(String schema) {
        jdbc.update(new SQLStatement(templates().setDefaultSchemas(schema)));
    }

    @Override
    public List<Warehouse> listSchemas(String schemaName, boolean onlyRegisteredAsWarehouses) {
        List<Warehouse> warehouses = jdbc.query(
            templates().listSchemas(schemaName),
            schemaName != null ? new Object[] {schemaName} : null,
            (ResultSet rs, int rownum) -> {
                String json = rs.getString("desc");
                SchemaComment comment = gsonSchema(json);
                String key = comment.getKey();
                String label = comment.getLabel();
                String schemaName1 = rs.getString("schema_name");
                if (key == null && schemaName1 != null) {
                    // SPACE_[PERSONAL_]$spaceKey|CONTEXT_$name
                    /*if (schemaName1.startsWith("space_")) {
                        String spaceKey = schemaName1.substring("space_".length());
                        key = "space:" + spaceKey;
                    } else if (schemaName1.startsWith("context_")) {
                        String contextName = schemaName1.substring("context_".length());
                        key = "name:" + contextName;
                    } else {
                        key = "name:schema_" + schemaName1;
                    }*/
                    /*if (label == null) {
                        label = "Warehouse " + key;
                    }*/
                }
                Warehouse warehouse = new Warehouse(
                    schemaName1,
                    key,
                    label);
                warehouse.setAvatarUrl(comment.getAvatarUrl());
                warehouse.setSpaceLabel(comment.getSpaceLabel());
                return warehouse;
            }
        );
        if (onlyRegisteredAsWarehouses) {
            Iterator<Warehouse> it = warehouses.iterator();
            while (it.hasNext())
                if (StringUtils.isBlank(it.next().getKey()))
                    it.remove();
        }
        return warehouses;
    }

    /**
     * Returns the warehouse. If it's the warehouse with which the connection was open, keep in mind that CNX_RW
     * creates the schema.
     *
     * @param key
     * @return
     */
    public Warehouse getWarehouse(String key) {
        // We have to deserialize every single schema, because... because the key isn't the schema name, but the
        // key is in the comments.
        List<Warehouse> warehouses = listSchemas(null, false);
        for (Warehouse warehouse : warehouses)
            if (equal(warehouse.getKey(), key))
                return warehouse;
        return null;
    }
/*
    @Override
    public Warehouse updateWarehouse(Warehouse warehouse) {
        jdbc.update(new SQLStatement(templates().updateSchemaComment(
            quoteString(gson(new SchemaComment(warehouse.getKey(), warehouse.getLabel(), warehouse.getAvatarUrl(), warehouse.getSpaceLabel()))),
            quoteEntityName(warehouse.getSchemaName())
        )));
        return warehouse;
    }*/

    @Override
    public Warehouse updateWarehouse(String schemaName, Consumer<Warehouse> modification) {
        if (StringUtils.isBlank(schemaName)) {
            throw new RuntimeException("Warehouse's schema name is empty: " + schemaName);
        }
        List<Warehouse> warehouses = listSchemas(schemaName, false);
        Warehouse warehouse;
        if (warehouses.size() > 1) {
            throw new RuntimeException("Several schemas with the same name? " + StringUtils.join(warehouses.stream().map((w) -> w.getSchemaName()).toArray(), ","));
        } else if (warehouses.size() == 1) {
            warehouse = warehouses.get(0);
        } else {
            warehouse = new Warehouse(schemaName, null, null);
        }
        modification.accept(warehouse);
        jdbc.update(new SQLStatement(templates().updateSchemaComment(
            quoteEntityName(warehouse.getSchemaName()),
            quoteString(gson(new SchemaComment(warehouse)))
        )));
        return warehouse;
    }

    @Override
    public List<String> getColumnList(String tableName) {
        return jdbc.query(
            templates().listColumns(),
            new Object[]{tableName},
            (rs, rowNum) -> {
                return rs.getString("COLUMN_NAME");
            });
    }

    @Override
    public DDLTable readTableDefinition(final String tablename, final boolean includeDeletedColumns) {
        return tableDefinitionCache.getOrSet(getTableDefinitionCacheKey(tablename), new CacheManager.InitialValue<DDLTable>() {
            @Override
            public DDLTable generate() {
                if (!hasTable(tablename))
                    return null;

                Tuple<String, String> tableCommentJson = jdbc.queryForOne(
                    templates().getCommentOnTable(),
                    new Object[]{tablename}, Tuple.class);
                if (tableCommentJson == null || tableCommentJson.first() == null) {
                    // The query is a left join. It should have at least returned the table.
                    return null;
                }
                boolean readwrite = tableCommentJson.second() != null;
                TableComment tableComment = gsonTable(tableCommentJson.second());
                String tableLabel = firstNonNull(tableComment.label(), tablename);
                List<TotalColumn> totals = tableComment.totals();
                List<DDLColumn> columnList = jdbc.query(
                    templates().listColumns(),
                    new Object[]{tablename},
                    new RowMapper() {
                        @Override
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            String name = rs.getString("COLUMN_NAME");
                            String typeString = rs.getString("DATA_TYPE");
                            String commentString = rs.getString("COM");
                            Integer length = PlaySqlUtils.parseInt(rs.getString("CHARACTER_MAXIMUM_LENGTH"));
                            DDLColumn.DataType type = parse(typeString, length).getOrElse(DDLColumn.DataType.UNKNOWN);
                            if (type != DDLColumn.DataType.VARCHAR) {
                                length = null;
                            }
                            ColumnComment columnComment = gsonColumn(commentString);
                            String label = firstNonNull(columnComment.label(), name);
                            String renderer = columnComment.renderer();
                            String viewRenderer = columnComment.viewRenderer();
                            String formula = columnComment.formula();
                            Integer width = columnComment.width();
                            Map<String, String> formats = columnComment.formats();

                            ForeignKey fk = columnComment.getFk();

                            return new DDLColumn(name, label, formula, renderer, viewRenderer, type, length, width, formats)
                                .setFk(fk);
                        }
                    });

                jdbc.query(
                    templates().listFks(true, false),
                    new Object[]{ tablename },
                    new RowMapper() {
                        @Override
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            String constraintName = rs.getString("constraint_name");
                            String columnName = rs.getString("column_name");
                            String fkTable = rs.getString("foreign_table_name");
                            String fkColumn = rs.getString("foreign_column_name");
                            String fkUpdateAction = rs.getString("confupdtype");
                            String fkDeleteAction = rs.getString("confdeltype");
                            String fkMatchType = rs.getString("confmatchtype");
                            for (DDLColumn column : columnList) {
                                if (equal(column.getName(), columnName)) {
                                    ForeignKey fk = column.getFk();
                                    if (fk == null) {
                                        fk = new ForeignKey(constraintName, fkTable, fkColumn, null, null, Action.parse(fkDeleteAction), true, true)
                                            .setOnUpdate(Action.parse(fkUpdateAction));
                                        column.setFk(fk);
                                    } else {
                                        fk.setConstraintName(constraintName);
                                        fk.setTargetTable(fkTable);
                                        fk.setTargetPK(fkColumn);
                                        fk.setWithSQLConstraint(true);
                                        fk.setOnDelete(Action.parse(fkDeleteAction));
                                        fk.setOnUpdate(Action.parse(fkUpdateAction));
                                    }
                                    return null;
                                }
                            }
                            // If we reach there, it's strange, because it means there's an unassigned
                            // fk declaration.
                            return null;
                        }
                    });
                Iterator<DDLColumn> columnIterator = columnList.iterator();
                if (!includeDeletedColumns)
                    while (columnIterator.hasNext())
                        if (columnIterator.next().getName().startsWith(AuditTrailItem.DELETED_PREFIX))
                            columnIterator.remove();

                // The first columns are ID and POSITION
                if (columnList.size() > 2 && (readwrite || equal(columnList.get(0).getName(), "ID") || equal(columnList.get(0).getName(), "PLAYSQL_ID"))) {
                    columnList.get(0).setStatusPK(true);
                }
                if (columnList.size() > 2 && (readwrite || equal(columnList.get(1).getName(), "POSITION") || equal(columnList.get(1).getName(), "PLAYSQL_POSITION"))) {
                    columnList.get(1).setStatusPosition(true);
                }

                DDLTable ddlTable = new DDLTable(readwrite, tablename, tableLabel, columnList.toArray(new DDLColumn[columnList.size()]))
                    .setTotals(totals)
                    .withColumnOrder(tableComment.columnOrder(), true);
                return ddlTable;
            }
        });
    }
    @Override
    public List<Tuple<DDLTable, DDLColumn>> getListLinkedTables(String tableName) {
        List<Tuple<String, String>> list = jdbc.query(templates().listFks(false, true),
            new Object[] { tableName },
            (RowMapper) (rs, rowNum) -> {
                String tableName1 = rs.getString("table_name");
                String columnName1 = rs.getString("column_name");
                return Tuple.of(tableName1, columnName1);
            });
        List<Tuple<DDLTable, DDLColumn>> foreignKeys = Lists.newArrayList();
        for (Tuple<String, String> item : list) {
            DDLTable table = this.readTableDefinition(item.getFirst(), false);
            if (table != null) {
                DDLColumn column = table.getColumnByName(item.getSecond());
                if (column != null) {
                    foreignKeys.add(Tuple.of(table, column));
                }
            }
        }
        return foreignKeys;
    }

    @Override
    public void fkAdd(DDLTable table, DDLColumn column, ForeignKey fk, String FK_RENDERER_KEY) {
        checkNotNull("Foreign key", fk);
        column.setFk(fk);
        column.setRenderer(FK_RENDERER_KEY);
        jdbc.execute(templates().fkAddToColumn(
            quoteEntityName(column.getName()),
            fkConstraint(column),
            quoteEntityName(table.getName())));

        setColumnComment(table, column);

        tableDefinitionCache.remove(getTableDefinitionCacheKey(table.getName()));
    }

    @Override
    public void fkRemove(DDLTable table, DDLColumn column, ForeignKey logicalFk) {
        ForeignKey fk = column.getFk();
        if (fk == null) throw new IllegalArgumentException("The column " + table.getName() + "." + column.getName() + " doesn't have a foreign key.");
        String fkName = fk.getConstraintName();
        if (fkName == null) throw new IllegalArgumentException("Can't find the name of the constraint for the column " + table.getName() + "." + column.getName() + ".");

        jdbc.execute(templates().fkRemove(quoteEntityName(fkName), quoteEntityName(table.getName())));

        if (logicalFk != null) {
            logicalFk.setWithSQLConstraint(false);
            logicalFk.setWithUI(true);
            column.setFk(logicalFk);
            setColumnComment(table, column);
        } else {
            ColumnComment comment = getCommentOnColumn(table.getName(), column.getName());
            comment.fkReset();
            comment.viewRenderer(null);
            comment.renderer(null);
            setColumnComment(table.getName(), column.getName(), comment);

            column.setFk(null);
        }

        tableDefinitionCache.remove(getTableDefinitionCacheKey(table.getName()));
    }

    /**
     * Before creating a foreign key, we must ensure all records have a matching value in the target table.
     *
     * @param originTable the origin table
     * @param originColumn the origin column, source of the FK
     * @param targetTable the target table (could be the same, for recursive FKs)
     * @param targetColumn the column where items should be inserted (could be the ID column or a display column).
     *                     Note: if this column is an INTEGER (for IDs, for example), then all non-integer values in
     *                     originColumn will be dropped.
     * @param otherTextColumn if targetColumn is the ID column, and you want to insert the value in both the ID and
     *                        display field, then please enter the display column's name in otherTextColumn.
     *                        Otherwise, null.
     *                        Do not use if 'otherTextColumn' is not of TEXT or VARCHAR type.
     */
    @Override
    public void fkInsertMissingValues(String originTable, DDLColumn originColumn, String targetTable, DDLColumn targetColumn, DDLColumn otherTextColumn) {
        boolean targetColumnIsNumber = isNumericType(targetColumn);
        boolean columnIsText = isTextType(originColumn);
        String targetColumnType = toSQLColumnType(new DDLColumn("temporary", "temporary", targetColumn.getType(), targetColumn.getLength())); // Returns TEXT, VARCHAR(5), SMALLINT, etc.
        jdbc.update(
            templates().fkInsertMissingValues(
                quoteEntityName(originTable),
                quoteEntityName(originColumn.getName()),
                quoteEntityName(targetTable),
                quoteEntityName(targetColumn.getName()),
                otherTextColumn != null ? quoteEntityName(otherTextColumn.getName()) : null,
                targetColumnType,
                targetColumnIsNumber,
                columnIsText));
    }

    @Override
    public void fkReplaceValueWithIDs(String originTable, DDLColumn originDisplayColumn, DDLColumn originIdColumn, String targetTable, DDLColumn targetDisplayColumn, DDLColumn targetIdColumn) {
        jdbc.update(
            templates().fkReplaceValueWithIDs(
                quoteEntityName(originTable),
                quoteEntityName(originDisplayColumn.getName()),
                quoteEntityName(originIdColumn.getName()),
                quoteEntityName(targetTable),
                quoteEntityName(targetDisplayColumn.getName()),
                quoteEntityName(targetIdColumn.getName())
            ));
    }

    @Override
    public void fkAddFkAndConvertValues(DDLTable originTable, DDLColumn originColumn, String targetTable, DDLColumn targetIdColumnDDL, DDLColumn targetDisplayColumnDDL,
                                             ForeignKey fk, String FK_RENDERER_KEY) {
        // 1. Rename the column
        // Move the old column aside, create a new one where we'll put
        // the PK values (because ADD FK USING ... doesn't accept subqueries)
        DDLColumn renamedOldColumn = originColumn.copy(null);
        String movedColumnName = null;
        for (int i = 0 ; i < 100 ; i++) {
            movedColumnName = "_temp_" + originColumn.getName() + (i == 0 ? "" : i);
            if (originTable.getColumnByName(movedColumnName) == null) {
                break;
            }
        }
        renamedOldColumn.setName(movedColumnName);
        renameColumn(originTable, originColumn, renamedOldColumn);

        // 2. Create the origin column
        // The following instruction has the added benefit of:
        // - Copying the metadata of the column
        // - Creating the FK at the SQL level
        originColumn.setType(DDLColumn.DataType.INTEGER);
        originColumn.setFk(fk);
        originColumn.setRenderer(FK_RENDERER_KEY);
        originColumn.setViewRenderer(null);
        addColumn(originTable, originColumn, renamedOldColumn);

        // 3. Transfer the values
        fkReplaceValueWithIDs(originTable.getName(), renamedOldColumn, originColumn, targetTable, targetDisplayColumnDDL, targetIdColumnDDL);

        // 4. Drop the renamedOldColumn
        // Normally, nothing should restrict the column from being dropped, so
        // abort if that's the case
        dropColumn(originTable, renamedOldColumn, DropBehaviour.RESTRICT);

        tableDefinitionCache.remove(getTableDefinitionCacheKey(originTable.getName()));
    }

    public void fkSaveLogicalDetails(DDLTable table, DDLColumn column, ForeignKey fk) {
        column.setFk(fk);
        setColumnComment(table, column);
    }

    private boolean isTextType(DDLColumn column) {
        switch (column.getType()) {
            case TEXT:
            case VARCHAR:
                return true;
        }
        return false;
    }

    private boolean isNumericType(DDLColumn column) {
        switch (column.getType()) {
            case INTEGER:
            case BIG_INTEGER:
            case NUMERIC:
            case DOUBLE:
                return true;
        }
        return false;
    }

    private String getTableDefinitionCacheKey(String tablename) {
        return jdbc().getDatasourceIdentifier() + "-" + tablename;
    }

    protected void setCommentOnTable(String tablename, TableComment comment) {
        String gson = gson(comment);
        jdbc.update(
            templates().setCommentOnTable(
                tablename,
                quoteString(gson)));
        tableDefinitionCache.remove(getTableDefinitionCacheKey(tablename));
    }

    @Override
    public void upgradeTask2_createFormulaDependenciesTable() {
        String sql = templates().createFormulaDependenciesTable();
        jdbc.execute(sql);
    }

    @Override
    public void upgradeTask2_upgradeExistingTables() {
        List<Tuple<String, String>> names = listPlaySqlTableNames(true);

        DDLColumn positionColumn = DDLColumn.getPositionColumnDDL();
        String definition = columnDefinition(positionColumn);

        for (Tuple<String, String> table : names) {
            jdbc.execute(templates().alterColumn(definition, quoteEntityName(table.first())));
        }
        tableDefinitionCache.removeAll();
    }

    @Override
    public void saveFormulaDependency(FormulaDependency item) {
        SQLStatement sql = new SQLStatement(
            templates().insertFormulaDependency(),
                item.origin().getTableName(),
                item.origin().getColumnName(),
                item.origin().getRowId(),
                item.destination().getTableName(),
                item.destination().getColumnName(),
                item.destination().getRowId(),
                item.destination().getFormula());
        jdbc.insert2(sql);
    }

    @Override
    public List<Endpoint> listFormulaDependencies(final Endpoint endpoint, FormulaDependency.EndpointRole role,
                                                  final boolean cascade,
                                                  final boolean includeSelf) throws CircularFormulaException {
        if (role != FormulaDependency.EndpointRole.ORIGIN) {
            throw new NotImplementedException("List formula dependencies for " + role.toString());
        }
        final List<Object> arguments = Lists.newArrayList();
        if (endpoint.getTableName() != null) arguments.add(endpoint.getTableName());
        if (endpoint.getColumnName() != null) arguments.add(endpoint.getColumnName());
        if (endpoint.getRowId() != null) arguments.add(endpoint.getRowId());
        if (cascade && includeSelf) {
            if (endpoint.getTableName() != null) arguments.add(endpoint.getTableName());
            if (endpoint.getColumnName() != null) arguments.add(endpoint.getColumnName());
            if (endpoint.getRowId() != null) arguments.add(endpoint.getRowId());
        }

        final long MAX_RECURSION = 1000L;

        SQLStatement sql = new SQLStatement(
            templates().listFormulaDependencies(MAX_RECURSION, cascade,
                endpoint.getColumnName() == null,
                includeSelf,
                endpoint.getRowId() == null,
                endpoint.getTableName() == null),
                arguments
                );

        return jdbc.query(sql, new RowMapper() {
            @Override
            public Endpoint mapRow(ResultSet rs, int rowNum) throws SQLException {
                if (rs.getLong("level") >= MAX_RECURSION) {
                    List<Object> arguments1 = Lists.newArrayList();
                    if (endpoint.getTableName() != null) arguments1.add(endpoint.getTableName());
                    if (endpoint.getColumnName() != null) arguments1.add(endpoint.getColumnName());
                    if (endpoint.getRowId() != null) arguments1.add(endpoint.getRowId());

                    String path = jdbc.queryForOne(new SQLStatement(
                        templates().formulaDependendenciesFindLoop(
                            MAX_RECURSION,
                            cascade,
                            endpoint.getColumnName() == null,
                            endpoint.getRowId() == null,
                            endpoint.getTableName() == null),
                            arguments1
                        ), new RowMapper(){
                        @Override
                        public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                            return rs.getString("path");
                        }
                    });

                    throw new CircularFormulaException(endpoint, path);
                }
                return new Endpoint(
                        orNull(rs.getString("tablename"), rs.wasNull()),
                        orNull(rs.getString("columnname"), rs.wasNull()),
                        orNull(rs.getLong("rowid"), rs.wasNull()),
                        orNull(rs.getString("formula"), rs.wasNull())
                );
            }
        });
    }

    @Override
    public void removeFormulaDependencies(Endpoint endpoint, FormulaDependency.EndpointRole role) {
        if (role != FormulaDependency.EndpointRole.DESTINATION) {
            throw new NotImplementedException("List formula dependencies for " + role.toString());
        }
        List<Object> arguments = Lists.newArrayList();
        if (endpoint.getTableName() != null) arguments.add(endpoint.getTableName());
        if (endpoint.getColumnName() != null) arguments.add(endpoint.getColumnName());
        if (endpoint.getRowId() != null) arguments.add(endpoint.getRowId());

        SQLStatement sql = new SQLStatement(
            templates().removeFormulaDependencies(
                endpoint.getColumnName() == null,
                endpoint.getRowId() == null,
                endpoint.getTableName() == null),
                arguments);

        jdbc.insert2(sql);
    }

    public void executeFormula(Endpoint endpoint, String sql) {
        SQLStatement stmt = new SQLStatement(
            templates().executeFormula(quoteEntityName(endpoint.getColumnName()), sql, quoteEntityName(endpoint.getTableName())),
                endpoint.getRowId());

        jdbc.insert2(stmt);
    }

    @Override
    public void renameColumnInFormulaDependencyTable(String localTableName, String oldColumnName, String newColumnName, List<Endpoint> endpointsToUpdate) {
        for(Endpoint endpoint : endpointsToUpdate) {
            SQLStatement stmt = new SQLStatement(
                templates().updateFormula(),
                    endpoint.getFormula(),
                    endpoint.getTableName(),
                    endpoint.getColumnName(),
                    endpoint.getRowId());
            jdbc.update(stmt);
        }

        jdbc.update(new SQLStatement(
            templates().updateFormulaRenameColumnOrigin(),
                newColumnName,
                localTableName,
                oldColumnName));
        jdbc.update(new SQLStatement(
            templates().updateFormulaRenameColumnDestination(),
            newColumnName,
            localTableName,
            oldColumnName));
    }

    @Override
    public void upgradeTask3_createEntitiesTable() {
        String sql = templates().createEntitiesTable();
        jdbc.execute(sql);
    }

    @Override
    public void upgradeTask5_createFilesTable() {
        String sql = templates().createFilesTable();
        jdbc.execute(sql);
    }

    //@Override
    public int saveFile(Integer id, Integer version, Integer scriptId, String type, String name, String content, Integer fileId, boolean create) {
        if(create) {
            SQLStatement statement = new SQLStatement(templates().createFile()).addArgument(version, scriptId, type, name, content, fileId);

            return jdbc.insertWithGeneratedKeys(statement);
        } else {
            SQLStatement sqlStatement = new SQLStatement(templates().updateFile());
            sqlStatement.addArgument(version, scriptId, type, name, content, fileId, id);

            jdbc.update(sqlStatement);
            return id;
        }
    }

    //@Override //TODO WARRNING !!! SAME AS SAVE WHY NOT USE BOOLEAN CREATE ?
//    public int updateFile(Integer id, Integer version, Integer scriptId, String type, String name, StringBuilder content, Integer fileId, boolean create) {
//        if(create) {
//            SQLStatement statement = new SQLStatement(templates().createFile()).addArgument(version, scriptId, type, name, content, fileId);
//
//            return jdbc.insertWithGeneratedKeys(statement);
//        } else {
//            SQLStatement sqlStatement = new SQLStatement(templates().updateFile());
//            sqlStatement.addArgument(id, version, scriptId, type, name, content, fileId);
//
//            jdbc.update(sqlStatement);
//            return id;
//        }
//    }

    //@Override
    public void deleteFile(int id) {
        SQLStatement sqlStatement = new SQLStatement(templates().deleteFile());
        sqlStatement.addArgument(id);

        jdbc.update(sqlStatement);
    }

    //@Override
    public List<String> getFile(int id) {
        SQLStatement sqlStatement = new SQLStatement(templates().getFile());
        sqlStatement.addArgument(id);

        return  jdbc.queryForOne(sqlStatement, new RowMapper() {
            @Override
            public List<String> mapRow(ResultSet rs, int i) throws SQLException {
                List<String> file = new ArrayList<>();
                file.add(rs.getString(1));// ID
                file.add(rs.getString(2));// Version
                file.add(rs.getString(3));// Script ID
                file.add(rs.getString(4));// Type
                file.add(rs.getString(5));// Name
                file.add(rs.getString(6));// Content
                file.add(rs.getString(7));// File ID
                return file;
            }
        });
    }

    //@Override
    public List<List<String>> readFilesTable() {
        SQLStatement sqlStatement = new SQLStatement(templates().readFilesTable());

        return jdbc.query(sqlStatement, new RowMapper() {
            @Override
            public List<String> mapRow(ResultSet rs, int rowNum) throws SQLException {
                List<String> file = new ArrayList<>();
                file.add(rs.getString(1));// ID
                file.add(rs.getString(2));// Version
                file.add(rs.getString(3));// Script ID
                file.add(rs.getString(4));// Type
                file.add(rs.getString(5));// Name
                file.add(rs.getString(6));// Content
                file.add(rs.getString(7));// File ID
                return file;
            }
        });
    }

    //@Override
    public List<List<String>> getAllScriptFiles(int scriptId) {
        SQLStatement sqlStatement = new SQLStatement(templates().getAllScriptFiles());
        sqlStatement.addArgument(scriptId);

        return jdbc.query(sqlStatement, new RowMapper() {
            @Override
            public List<String> mapRow(ResultSet rs, int rowNum) throws SQLException {
                List<String> file = new ArrayList<>();
                file.add(rs.getString(1));// ID
                file.add(rs.getString(2));// Version
                file.add(rs.getString(3));// Script ID
                file.add(rs.getString(4));// Type
                file.add(rs.getString(5));// Name
                file.add(rs.getString(6));// Content
                file.add(rs.getString(7));// File ID
                return file;
            }
        });
    }

    @Override
    public int saveEntity(EntityType type, Integer id, String key, String label, String lastModified, String sql, String serialized, boolean create) {
        if (create) {
            SQLStatement statement = new SQLStatement(templates().insertEntity())
                    .addArgument(type.toString(), key, label, lastModified, sql, serialized);

            return jdbc.insertWithGeneratedKeys(statement);
        } else {
            SQLStatement statement = new SQLStatement(templates().updateEntity(id != null, false))
                    .addArgument(type.toString(), key, label, lastModified, sql, serialized, firstNonNull(id, key));
            jdbc.update(statement);
            return id;
        }
    }

    /**
     * All entities... except spreadshee
     * @return
     */
    public List<Truple<Integer, EntityType, String>> getAllEntityNamesExceptSS() {
        SQLStatement sqlStatement = new SQLStatement(templates().readEntity(false, false, false, false));
        return jdbc.query(sqlStatement, (rs, numRow) ->
                Truple.of(rs.getInt(1), EntityType.valueOf(rs.getString(2)), rs.getString(3)));
    }

    @Override
    public CapabilityEntities.Entity getEntity(EntityType type, int id) {
        SQLStatement sqlStatement = new SQLStatement(templates().readEntity(true, false, type != null, true));
        if (type != null) sqlStatement.addArgument(type.toString());
        sqlStatement.addArgument(id);
        return jdbc.queryForOne(sqlStatement, (rs, numRow) ->
                CapabilityEntities.Entity.of(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4)));
    }

    @Override
    public CapabilityEntities.Entity getEntity(EntityType type, String key) {
        SQLStatement sqlStatement = new SQLStatement(templates().readEntity(false, true, type != null, true));
        if (type != null) sqlStatement.addArgument(type.toString());
        sqlStatement.addArgument(key);

        return jdbc.queryForOne(sqlStatement, (rs, rowNum) -> {
            return CapabilityEntities.Entity.of(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
        });
    }

    @Override
    public List<CapabilityEntities.Entity> getEntities(EntityType type) {
        SQLStatement sqlStatement = new SQLStatement(templates().readEntity(false, false, type != null, true));
        if (type != null) sqlStatement.addArgument(type.toString());

        return jdbc.query(sqlStatement, new RowMapper() {
            @Override
            public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                // 1: id
                // 2: entity_type
                // 3: label
                // 4: serialized json
                return CapabilityEntities.Entity.of(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
            }
        });
    }


    @Override
    public void deleteEntity(EntityType type, int id) {
        SQLStatement sqlStatement = new SQLStatement(templates().deleteEntity(true, type != null));
        if (type != null) sqlStatement.addArgument(type.toString());
        sqlStatement.addArgument(id);

        jdbc.update(sqlStatement);
    }

    /** Executes an upgrade task for the POSITION column.
     * Flaw: Only executes it for the current space. If a formula in another
     * space depends on it, we're doomed.
     * @param includeNulls
     * @param transformation
     */
    @Override
    public void executeRowMetadataUpgradeTask(boolean includeNulls, String targetTableName, Function<Tuple<String, String>, String> transformation) {
        List<Tuple<String, String>> tableNames;
        if (targetTableName != null) {
            tableNames = Lists.newArrayList(Tuple.of(targetTableName, targetTableName));
        } else {
            tableNames = listPlaySqlTableNames(true);
        }
        String idColumnName = quoteEntityName("ID");
        String positionColumnName = quoteEntityName("POSITION");

        for (Tuple<String, String> table : tableNames) {
            String tableName = table.first();
            String quotedTableName = quoteEntityName(table.first());
            long RECORD_LIMIT = 500;
            // Munch result sets until there are not enough records to reach RECORD_LIMIT in the result set.
            long offset = 0;
            while (true) {
                SQLStatement sql = new SQLStatement(
                    templates().readData(idColumnName + ", " + positionColumnName,
                        RECORD_LIMIT, offset, null, tableName, includeNulls ? null : positionColumnName + " IS NOT NULL"));
                // Fetch
                List<Tuple<Long, String>> idAndPositionList = jdbc.query(sql, new RowMapper() {
                    @Override
                    public Tuple<Long, String> mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new Tuple<>(rs.getLong(1), rs.getString(2));
                    }
                });
                // Transform
                List<Tuple<Long, String>> updates = Lists.newArrayList();
                for (Tuple<Long, String> idAndPosition : idAndPositionList) {
                    Tuple<String, String> tableAndMetadata = Tuple.of(tableName, idAndPosition.second());
                    String newMetadata = transformation.apply(tableAndMetadata);
                    if (!equal(newMetadata, idAndPosition.second())) {
                        updates.add(Tuple.of(idAndPosition.first(), newMetadata));
                    }
                }
                // Update
                // TODO That potentially makes a batch of 500 updates. Any way to bulk-update them?
                for (Tuple<Long, String> tuple : updates) {
                    SQLStatement sqlUpdate = new SQLStatement(templates().writeData(Lists.newArrayList(positionColumnName), tuple.first(), quotedTableName))
                        .addArgument(tuple.second());
                    jdbc.update(sqlUpdate);
                }
                // We quit when one result set is not full
                if (idAndPositionList.size() != RECORD_LIMIT) break;
                offset += RECORD_LIMIT;
            }
        }
        tableDefinitionCache.removeAll();
    }

    /**
     * For the time being, it just returns an informational type
     * @param sqlType the java.sql.Types
     * @return the Play SQL type
     */
    @Override
    public DDLColumn.DataType fromSqlType(Integer sqlType) {
        if (sqlType == null) return null;
        switch (sqlType) {
            case Types.BIT: return DDLColumn.DataType.BOOLEAN; // This is a weirdness of Postgresql
            case Types.TINYINT: return DDLColumn.DataType.INTEGER;
            case Types.SMALLINT: return DDLColumn.DataType.INTEGER;
            case Types.INTEGER: return DDLColumn.DataType.INTEGER;
            case Types.BIGINT: return DDLColumn.DataType.INTEGER;
            case Types.FLOAT: return null;
            case Types.REAL: return null;
            case Types.DOUBLE: return DDLColumn.DataType.DOUBLE;
            case Types.NUMERIC: return DDLColumn.DataType.NUMERIC;
            case Types.DECIMAL: return DDLColumn.DataType.NUMERIC;
            case Types.CHAR: return DDLColumn.DataType.VARCHAR;
            case Types.VARCHAR: return DDLColumn.DataType.VARCHAR;
            case Types.LONGVARCHAR: return DDLColumn.DataType.TEXT;
            case Types.DATE: return DDLColumn.DataType.DATE;
            case Types.TIME: return DDLColumn.DataType.TIME;
            case Types.TIMESTAMP: return DDLColumn.DataType.TIMESTAMP;
            case Types.BINARY: return null;
            case Types.VARBINARY: return null;
            case Types.LONGVARBINARY: return null;
            case Types.NULL: return null;
            case Types.OTHER: return null;
            case Types.JAVA_OBJECT: return null;
            case Types.DISTINCT: return null;
            case Types.STRUCT: return null;
            case Types.ARRAY: return null;
            case Types.BLOB: return null;
            case Types.CLOB: return null;
            case Types.REF: return null;
            case Types.DATALINK: return null;
            case Types.BOOLEAN: return DDLColumn.DataType.BOOLEAN;
            case Types.ROWID: return null;
            case Types.NCHAR: return DDLColumn.DataType.VARCHAR;
            case Types.NVARCHAR: return DDLColumn.DataType.VARCHAR;
            case Types.LONGNVARCHAR: return DDLColumn.DataType.TEXT;
            case Types.NCLOB: return null;
            case Types.SQLXML: return null;
            default: return null;
        }
    }

    @Override
    public int toSqlType(DDLColumn.DataType type) {
        switch (type) {
            case BOOLEAN: return Types.BOOLEAN;
            case DATE: return Types.DATE;
            case NUMERIC: return Types.DECIMAL;
            case DOUBLE: return Types.DOUBLE;
            case INTEGER: return Types.INTEGER;
            // Sigh
            case INTERVAL: return Types.INTEGER;
            case TIME: return Types.TIME;
            case TIMESTAMP: return Types.TIMESTAMP;
            case VARCHAR: return Types.VARCHAR;
            case TEXT: return Types.LONGVARCHAR;
            case BYTEA: return Types.BINARY;
            default:
                throw new NotImplementedException(type.toString());
        }
    }

    protected Option parse(String dataType, Integer length) {
        if ("VARCHAR".equals(dataType))
            return some(DDLColumn.DataType.VARCHAR);
        if ("NUMERIC".equals(dataType))
            return some(DDLColumn.DataType.NUMERIC);
        if ("FLOAT8".equals(dataType))
            return some(DDLColumn.DataType.DOUBLE);
        if ("INTEGER".equals(dataType))
            return some(DDLColumn.DataType.INTEGER);
        if ("BIGINT".equals(dataType))
            return some(DDLColumn.DataType.INTEGER);
        if ("BOOLEAN".equals(dataType))
            return some(DDLColumn.DataType.BOOLEAN);
        if ("DATE".equals(dataType))
            return some(DDLColumn.DataType.DATE);
        if ("INTERVAL".equals(dataType))
            return some(DDLColumn.DataType.INTERVAL);
        if ("TIME".equals(dataType))
            return some(DDLColumn.DataType.TIME);
        if ("TIMESTAMP".equals(dataType))
            return some(DDLColumn.DataType.TIMESTAMP);
        if ("TEXT".equals(dataType))
            return some(DDLColumn.DataType.TEXT);
        if ("BYTEA".equals(dataType))
            return some(DDLColumn.DataType.BYTEA);

        return none();
    }

    public String columnDefinition(DDLColumn column) {
        /**
         * column_name data_type [ CONSTRAINT constraint_name ]
         * { NOT NULL |
         * NULL |
         * CHECK ( expression ) |
         * DEFAULT default_expr |
         * UNIQUE index_parameters |
         * PRIMARY KEY index_parameters |
         * REFERENCES reftable [ ( refcolumn ) ] [ MATCH FULL | MATCH PARTIAL | MATCH SIMPLE ]
         * [ ON DELETE action ] [ ON UPDATE action ] }
         * [ DEFERRABLE | NOT DEFERRABLE ] [ INITIALLY DEFERRED | INITIALLY IMMEDIATE ]
         */
        String columnName = column.getName();
        String dataType = toSQLColumnType(column);

        return concatOptional(quoteEntityName(columnName), dataType);
    }

    protected String fkConstraint(DDLColumn column) {
        // alter table test2 add foreign key (fk3) references test1;
        ForeignKey fk = column.getFk();
        if (fk != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("REFERENCES ").append(quoteEntityName(fk.getTargetTable()));
            if (fk.getOnDelete() != null) {
                String translation = null;
                sb.append(" ON DELETE ").append(fk.getOnDelete().getSqlExpression());
                if (fk.getOnUpdate() == null) {
                    sb.append(" ON UPDATE ").append(fk.getOnDelete().getSqlExpression());
                } else {
                    sb.append(" ON UPDATE ").append(fk.getOnUpdate().getSqlExpression());
                }
            }
            return sb.toString();
        }
        return null;
    }

    protected String toSQLColumnType(DDLColumn column) {
        String options = "";
        if (column.has(IDENTITY))
            options += " IDENTITY";
        if (column.has(NOT_NULL))
            options += " NOT NULL";

        switch (column.getType()) {
            case VARCHAR:
                return "VARCHAR(" + column.getLength() + ")" + options;
            case NUMERIC:
                // NUMERIC [( precision, scale )], e.g. 12.3456 is NUMERIC ( 6, 4 ). Default (precision, scale) means there's no constraint.
                return "NUMERIC" + options;
            case DOUBLE:
                return "FLOAT8" + options;
            case INTEGER:
                return "INTEGER" + options;
            case BIG_INTEGER:
                return "BIGINT" + options;
            case BOOLEAN:
                return "BOOLEAN" + options;
            case DATE:
                return "DATE" + options;
            case INTERVAL:
                return "INTERVAL " + column.getIntervalDetail() + options;
            case TIME:
                return "TIME" + options;
            case TIMESTAMP:
                return "TIMESTAMP" + options;
            case TEXT:
                return "TEXT";
            case BYTEA:
                return "BYTEA";
        }
        throw new NotImplementedException();
    }

    protected String concatOptional(String... arguments) {
        List<String> argumentList = Lists.newArrayList();
        for (String argument : arguments)
            if (argument != null)
                argumentList.add(argument);
        return StringUtils.join(argumentList, " ");
    }

    public String quoteString(String string) {
        // Replace: CR, LF, field, tab, \ and '
        // With: \n, \r, \f, \t, \\ and \'
        int length = string.length();
        StringBuilder sb = new StringBuilder(length * 2 + 3);
        for (int i = 0 ; i < length ; i++) {
            switch (string.charAt(i)) {
                case '\n': sb.append("\\n"); break;
                case '\r': sb.append("\\r"); break;
                case '\f': sb.append("\\f"); break;
                case '\t': sb.append("\\t"); break;
                case '\\': sb.append("\\\\"); break;
                case '\'': sb.append("''"); break;
                default: sb.append(string.charAt(i));
            }
        }
        sb.insert(0, "E'");
        sb.append("'");
        return sb.toString();
    }
}